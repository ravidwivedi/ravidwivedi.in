---
title: "My Austrian Visa Refusal Story"
date: 2024-08-11T11:59:04+05:30
draft: false
type: "post"
tags: ["vfs-global", "austria", "visa", "schengen-visa"]
showTableOfContents: false
---
Vienna - the capital of Austria - is one of the most visited cities in the world, popular for its rich history, gardens, and cafes, along with well-known artists like Beethoven, Mozart, Gödel, and Freud. It has also been consistently ranked as the most livable city in the world.

For these reasons, I was elated when my friend [Snehal](https://inferred.in) invited me last year to visit Vienna for a few days. We included Christmas and New Year's Eve in my itinerary due to the city's popular Christmas markets and lively events. The festive season also ensured that Snehal had some days off for sightseeing.

Indians require a visa to visit Austria. Since the travel dates were near, I rushed to book an appointment online with VFS Global in Delhi, and quickly arranged the required documents. However, at VFS, I found out that I had applied in the wrong appointment category (tourist), which depends on the purpose of the visit, and that my travel dates do not allow enough time for visa authorities to make a decision. Apparently, even if you plan to stay only for a part of the trip with the host, you need to apply under the category "Visiting Friends and Family".

Thus, I had to book another appointment under this category, and took the opportunity to shift my travel dates to allow at least 15 business days for the visa application to be processed, removing Christmas and New Year's Eve from my itinerary.

The process went smoothly, and my visa application was submitted by VFS. For reference, here's a list of documents I submitted -

- VFS appointment letter

- Duly-filled visa application form

- Original passport

- Copy of passport

- 1 photograph

- My 6 months bank account statement

- Cover letter

- Consent form (that visa processing will take up to 15 business days)

- Snehal's job contract

- My work contract

- Rent contract of Snehal

- Residence permit of Snehal

- A copy of Snehal's passport

- Invitation letter from Snehal

- Return flight ticket reservations

- Travel insurance for the intended travel dates

The following charges were collected from me.

| **Service Description**                       | **Amount (Indian Rupees)** |
|-----------------------------------------------|-----------------------------:|
| Cash Handling Charge - SAC Code: (SAC:998599) |                           0 |
| VFS Fee - India - SAC Code: (SAC:998599)      |                        1,820 |
| VISA Fee - India - SAC Code:                  |                        7,280 |
| Convenience Fee - SAC Code: (SAC:998599)      |                          182 |
| Courier Service - SAC Code: (SAC:998599)      |                          728 |
| Courier Assurance - SAC Code: (SAC:998599)    |                          182 |
| **Total**                                     |                     **10,192** |

I later learned that the courier charges (728 INR) and the courier assurance charges (182 INR) mentioned above were optional. However, VFS didn't ask whether I wanted to include them. When the emabssy is done processing your application, it will send your passport back to VFS, from where you can either collect it yourself or get it couriered back home, which requires you to pay courier charges. However, courier assurance charges do not add any value as VFS cannot "assure" anything about courier and I suggest you get them removed.

My visa application was submitted on the 21st of December 2023. A few days later, on the 29th of December 2023, I received an email from the Austrian embassy asking me to submit an additional document -

> Subject: AUSTRIAN VISA APPLICATION - AMENDMENT REQUEST: Ravi Dwivedi VIS 4331
> 
> Dear Applicant,
> 
> On 22.12.2023 your application for Visa C was registered at the Embassy. You are requested to kindly send the scanned copies of the following documents via email to the Embassy or submit the documents at the nearest VFS centre, for further processing of your application:
> 
> 
>   *   Kindly submit Electronic letter of guarantee "EVE- Elektronische Verpflichtungserklärung" obtained from the "Fremdenpolizeibehörde" of the sponsor's district in Austria. Once your host company/inviting company has obtained the EVE, please share the reference number (starting from DEL_____) received from the authorities, with the Embassy.

I misunderstood the required document (the EVE) to be a scanned copy of the letter of guarantee form signed by Snehal, and responded by attaching it.

Upon researching, Snehal determined that the document is an electronic letter of guarantee, and is supposed to be obtained at a local police station in Vienna. He visited a police station the next day and had a hard time conversing due to the language barrier (German is the common language in Austria, whereas Snehal speaks English). That day was a weekend, so he took an appointment for Monday, but in the meantime the embassy had finished processing my visa.

My visa was denied, and the refusal letter stated:

> The Austrian embassy in Delhi examined your application; the visa has been refused.
> 
> The decision is based on the following reason(s):
> 
> - The information submitted regarding the justification for the purpose and conditions of the intended stay was not reliable.
> 
> - There are reasonable doubts as to your intention to leave the territory of the Member States before the expiry of the visa.
> 
> Other remarks:
> 
> You have been given an amendment request, which you have failed to fulfil, or have only fulfilled inadequately, within the deadline set.
> 
> You are a first-time traveller. The social and economic roots with the home country are not evident. The return from Schengen territory does therefore not seem to be certain.

I could have reapplied after obtaining the EVE, but I didn't because I found the following line
> The social and economic roots with the home country are not evident.

offensive for someone who was born and raised in India, got the impression that the absence of electronic guarantee letter was not the only reason behind the refusal, had already wasted 12,000 INR on this application, and my friend's stay in Austria was uncertain after January. In fact, my friend soon returned to India.

To summarize -
1. If you are visiting a host, then the category of appointment at VFS must be "Visiting Friends and Family" rather than "Tourist".
2. VFS charged me for courier assurance, which is an optional service. Make sure to get these removed from your bill. 
3. Neither my travel agent nor the VFS application center mentioned the EVE.
4. While the [required documents list](https://visa.vfsglobal.com/one-pager/austria/india/english/pdf/Visa-for-Visitors-Document.pdf) from the VFS website does mention it in point 6, it leads to a [dead link](http://www.bmi.gv.at/cms/BMI_Fremdenpolizei/einreise_visa/Visum_6.aspx).
5. Snehal informed me that a mere two months ago, his wife's visa was approved without an EVE. This hints at inconsistency in processing of applications, even those under identical categories.

Such incidents are a waste of time and money for applicants, and an embarrassment to VFS and the Austrian visa authorities. I suggest that the Austrian visa authorities fix that URL, and provide instructions for hosts to obtain the EVE. 


**Credits to Snehal and [Contrapunctus](https://contrapunctus.codeberg.page/
) for editing, Badri for proofreading.**
