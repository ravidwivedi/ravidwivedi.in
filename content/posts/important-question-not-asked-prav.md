---
title: "Nobody asked this important question about Prav"
date: 2023-03-24T13:35:25+05:30
draft: false
type: "post"
showTableOfContents: true
tags: ["prav", "politics"]
---
Earlier this month, I gave a presentation introducing [Prav](https://prav.app) along with my friend Arun. Many people got curious and asked various questions about the project. However, I was surprised that nobody inquired about the operation of a privacy messaging service in India, especially considering the weakening privacy laws in the country as highlighted in this [article](https://scroll.in/article/1010778/how-the-modi-governments-new-it-rules-jeopardise-the-right-to-privacy-and-free-speech).

The conference where I presented had at least two talks/discussions on the policy/law aspects of technology. Even outside the conference, I did not encounter this question, which I believe is crucial. It seems that people are not yet fully aware of the implications of the new IT rules.

**Update on 27 May 2023:** This question was later [raised on social media](https://mstdn.social/@sachinsaini/110411423706685630), reminding me of a similar question asked [here](https://metalhead.club/@akash/109527124440791656) before I wrote this post.
