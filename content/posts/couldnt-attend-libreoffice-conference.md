---
title: "Couldn't Attend Libreoffice Conference 2022"
date: 2022-10-04T19:29:56+05:30
draft: false
type: "post"
showTableOfContents: true
tags: ["travel", "vfs-global", "visa", "LibreOffice", "conference", "schengen-visa"]
---
LibreOffice is a [freedom-respecting](https://www.gnu.org/philosophy/free-software-even-more-important.html) office suite offering word processors, spreadsheets, and presentations. More importantly, it is a community-driven project backed by the non-profit organization [The Document Foundation](https://www.documentfoundation.org/), registered in Germany. I have promoted LibreOffice by marketing the office suite in India. The LibreOffice community organizes an [annual conference](https://conference.libreoffice.org/2022/) of community members, which I was invited to attend, along with a generous offer to cover all my conference-related expenses. This year, the conference was held in Italy.

In order to attend the conference, I applied for a visa to Italy, which requires visiting the VFS Global application center along with the required documents. Upon booking my appointment, I found out that they were so scarce that it took me eight days to get one. However, when I went to the VFS center, they told me I had taken an appointment in the wrong category (the tourist category is not appropriate for attending conferences), and I should have chosen either the “invitation” or “business” category. So, I was asked to book another appointment under an appropriate category. However, I didn’t get another appointment with VFS at the Delhi center. The VFS centers where appointment slots were available were very far away from me, so I didn’t book an appointment there.

In terms of learning from the situation, you should book your visa appointment at VFS in the appropriate category.
