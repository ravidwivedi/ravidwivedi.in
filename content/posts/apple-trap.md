---
title: "Apple's privacy trap"
date: 2021-07-13
tags: ["Apple", "Privacy", "FreeSoftware"]
type: "post"
showTableOfContents: true
---
Apple is a company which [claims that protecting users' privacy is one of their core values](https://www.apple.com/privacy/). Recently, they announced a new software update to their iOS with the idea that the apps made by independent developers have to [request explicit permission from users before tracking them](https://www.inc.com/jason-aten/apples-privacy-update-is-turning-out-to-be-worst-case-scenario-for-facebook.html). 

That means Apple cares about users' privacy. 

Or do they?

Sure, they are not involved in [targeted advertising](https://en.wikipedia.org/wiki/Targeted_advertising), and they don't earn money from your data and therefore, do not track you, right? 

And they [encrypt all contents of iOS devices](https://arstechnica.com/gadgets/2014/09/apple-expands-data-encryption-under-ios-8-making-handover-to-cops-moot/), making it impossible for third-parties to access your data, right?  

They are working day and night to make your life better, convenient and at the same time, defend your privacy. What more can we ask for?

Well ..... we don't know if any of these claims are true

because Apple's iOS is a [nonfree/proprietary program](https://gnu.org/malware), and therefore these claims cannot be independently verified. Apple does not give its users the freedom to study how iOS works-- it does not provide its [source code](https://en.wikipedia.org/wiki/Source_code).

<figure>
  <img src="/images/proprietary.jpg" alt="Cartoon: A user is being walked on a leash. The leash is held by proprietary software." width="300" height="300" >
  <figcaption> Proprietary Software controls the user.  Source: <a href="https://static.fsf.org/nosvn/RMS_Intro_to_FS_TEDx_Slideshow.odp">Richard Stallman's Ted talk slides</a> released under CC-BY 3.0 license.</figcaption>
</figure>


<br>

Even if Apple's claims are true, this only means you get privacy from others and not from Apple. They might be encrypting all your contents stored in iOS, we don't know, but [they have also installed backdoors](https://gizmodo.com/report-claims-apple-left-a-backdoor-open-at-fbis-reques-1841135460). This means Apple can make changes in your devices remotely without your permission. They can add any malicious features in the software at any time without you knowing. The backdoors were found in [macOS as well as in the iOS](https://www.gnu.org/proprietary/malware-apple.html#back-doors). That means [your computer isn’t yours](https://sneak.berlin/20201112/your-computer-isnt-yours/).

While Apple says that it respects its user's privacy, their track-record says exactly the opposite.

Snowden's documents show that [Apple is a part of NSA's global surveillance program](https://eu.usatoday.com/story/news/2013/06/06/nsa-surveillance-internet-companies/2398345/). 

In China, it stores [all user data on servers controlled by the Chinese government](https://www.cpomagazine.com/data-privacy/icloud-data-turned-over-to-chinese-government-conflicts-with-apples-privacy-first-focus/).

All the search queries made in macOS' spotlight [are send to Apple](http://finance.yahoo.com/blogs/the-exchange/privacy-advocates-worry-over-new-apple-iphone-tracking-feature-161836223.html).

Apple [uploaded private files](https://www.washingtonpost.com/news/the-switch/wp/2014/10/30/how-one-mans-private-files-ended-up-on-apples-icloud-without-his-consent/) of its user to iCloud without consent. 

You remember who is Siri? Siri is Apple's voice assistant. It is Apple's voice assistant, not your voice assistant. You might think that Siri listens to you only when prompted, but it turns out that [it listens and records all the time](https://www.theguardian.com/technology/2020/may/20/apple-whistleblower-goes-public-over-lack-of-action). Apple also hires people specifically for [listening to users’ Siri recordings](https://www.theguardian.com/technology/2020/may/20/apple-whistleblower-goes-public-over-lack-of-action).

You can check out more cases of surveillance by Apple by visiting [this link](https://www.gnu.org/proprietary/malware-apple.html#surveillance).

Cory Doctorow argues that surrendering your autonomy by moving to Apple's fortress has the same problem as all benevolent dictatorships: [it works well, but fails badly](https://locusmag.com/2021/01/cory-doctorow-neofeudalism-and-the-digital-manor/).  Let me quote Cory Doctorow from [this article](https://pluralistic.net/2021/06/08/leona-helmsley-was-a-pioneer/#manorialism), 

> Apple rightly points out that the world is full of bandits who will steal your data and money and ruin your life, and it holds itself out as your protector .... But when Apple sides with the bandits, the walls 
that once protected you now make you easy prey.

Overall, Apple claims itself to be the guardian protecting its users' privacy, while their track record is exactly the opposite, they [increased surveillance on users](https://www.fsf.org/news/the-problems-with-apple-arent-just-outages-they-are-injustices) with the latest macOS update, they won't allow third-party repair shops but their "authorized" technicians themselves [leak users' data](https://www.theguardian.com/technology/2021/jun/07/apple-settles-iphone-explicit-images). Apple's privacy claims, at their best, only give users privacy from third-parties. That is what their privacy trap is-- Redefine privacy to mean privacy from others and earn goodwill by advertising that they care about users' privacy. Surveillance is just a symptom of proprietary software. The bigger problem is that Apple keeps tight control over iDevices and users don't control them. 

The only software we can trust is [free software](https://www.gnu.org/philosophy/free-software-even-more-important.html), which respects user's freedom. Users have control over the software if it is free software. If the software is free, users can check for malicious code and remove the malicious functionalities. 

<figure>
  <img src="/images/four_freedoms.png" alt="Four Freedoms of free software" width="300" height="300" >
  <figcaption> Free Software respects your freedom. Source: <a href="https://static.fsf.org/nosvn/RMS_Intro_to_FS_TEDx_Slideshow.odp">Richard Stallman's Ted talk slides</a> released under CC-BY 3.0 license.</figcaption>
</figure>

A nonfree software, on the other hand, is [malware](https://gnu.org/malware). Apple's iOS is malware because it is nonfree software. It is a black box. We never know what it is up to, so any trust on Apple respecting user's privacy is a blind trust. 
