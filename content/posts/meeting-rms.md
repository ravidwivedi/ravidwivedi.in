---
title: "My Meeting with Richard Stallman"
draft: false
type: "post"
date: 2021-07-31
showTableOfContents: true
---
I am writing about a meeting which happened more than 5 years before writing this article. This is the best recreation from memory I came up with.

At the time this happened, I was a student at Acharya Narendra Dev College (also known as "ANDC" in short), New Delhi. On the 1st of March 2016, the principal of my college, Dr. Savithri Singh, was addressing all the students in a common hall (for some reason I don't remember). During the address, she told us that the founder of Linux was visiting our college that day. She invited interested students to meet him to her office at 4 PM. I was sitting with my friend, Shivam Rai who was excited for the meeting. I am from mathematics background and hardly knew anything about Linux. I had heard the word "Linux" and knew that it is a kernel of some operating system. So, I wasn't very entusiastic. 

Around 4 PM, I was a bit tired and wanted to go back to my room to take a nap. I was not completely sure about the point of the meeting. Furthermore, I thought they discuss programming or coding like mathematicians discuss theorems and proofs. I almost forgot about it. However, my friend reminded me of the meeting, so I joined him. 

We went to the principal office and after waiting for some time, the visitor arrived along with a person from Kerala. My friend and I shook hands with the visitor, and he said, "Hi, my name is Richard Stallman". We also introduced ourselves. The person from Kerala also introduced himself but unfortunately, I forgot his name. Immediately, Richard Stallman asked us a question in a joking manner, "If I were to start a company in India, what should I name it?". We were silent. Then he said, "Mahadeva" followed by a big laughter. 
It turned out that, even though the principal invited the whole college to meet Richard Stallman, only two of us actually went to meet him. Next, since the principal said that he is a founder of Linux, we started the discussion something along the lines of "You are the founder of Linux?" Listening to this, he got angry and said, "You should ask that question from Linus Torvalds." I was confused at this point, and so was my friend. 

Then we asked, "What do you work for?". He responded, "I work for GNU." I asked, "What's that?" He said, "GNU's not Unix." I got very confused at this point as I didn't know what's GNU, what's Unix, and we already pissed him off by associating him with Linux. 

Then, he said, "I work for free software." This added to my confusion as I thought why would someone work for free-of-cost software. Then he explained what is free software, "Free Software is software that gives users four freedoms.

Freedom 0 is the freedom to run the software as you wish.

Freedom 1 is the freedom to study and modify the program. You need to have the source code of the program to exercise this freedom.

Freedom 2 is the freedom to share the program.

Freedom 3 is the freedom to share your modified versions.

The 'free' does not mean free of cost. It means freedom. Think of it like free speech and not free beer."

At that point, I only knew that a lot of programs do not have freedom 1. I didn't know that a lot of software do not give users the freedom to share or run. So naturally, I didn't understand why he has to emphasize these freedoms and why they are important. Also, I used to think every software have freedoms 0,2,3. I remember having a (wrong) notion  that the freedom to study and modify the source code is for the programmers only. 

Then, he said, "Any software which does not give users these four freedoms is called a nonfree/proprietary software. A proprietary software is under developer's control, and you are at the mercy of the developer. A free software is controlled by its users. I use only free software and reject all proprietary software and, you should do it too."

His voice was quite passionate when he was telling us those things. Further, he told us that Microsoft Windows is an example of a proprietary software and mentioned that a backdoor was found in Windows in the past. I didn't know what backdoor means and I don't remember if he told us that.

He had hearing problems, so we had to be loud while interacting with him. My friend then asked about Apple's software. Something like "Is Apple's software secure?" Stallman replied something related to Apple's locking of user and Apple's software being nonfree. Bu I forgot a lot of at he said.

I remember that he also said that Google services are surveillance systems, and he does not use them. He insisted that we should also refuse to use them. I remember him mentioning that NSA spies on Americans as well as other countries' citizens using Google services like Gmail, Google Maps. Poor me, I didn't know what is NSA.

He also mentioned that Google Maps does not load if he disables the Javascript code sent by the site. Earlier, it used to load without Javascript. He kind of implied that sites should load without Javascript. And he probably gave a few more examples when disabling Javascript does not load the website. At that time, I didn't understand why would someone disable Javascript when visiting a website.

Then, our principal reminded us to take photos with Richard Stallman. Stallman agreed with the condition that we never upload his photo on Facebook, Instagram or WhatsApp, because that company is a surveillance engine, and we should not feed it. We agreed with the condition and clicked photos with him. 

<figure>
<img src="/images/photo-with-rms.jpg" width=400 height=300>
<figcaption>My photo with RMS</figcaption>
</figure>

Richard Stallman was then treated with Dal Vada from our college canteen, and he really liked that.

This meeting planted a few questions in my mind about the software I was using. At that time, I didn't have adequate political understanding to get his point. I also found it a bit sad that the person is known for "Linux" and "Open Source", even though he despises those labels.
