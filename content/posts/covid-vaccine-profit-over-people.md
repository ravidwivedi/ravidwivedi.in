---
title: Covid Vaccine- A Case Of Prioritizing Greed Of A Few Over Public Health 
date: 2021-12-04
draft: false
type: "post" 
showTableOfContents: true
---
Look at the image below:

<figure>
<img src="/images/World_map_of_share_of_the_population_fully_vaccinated_against_COVID-19_by_country.png" width="500" height="300">
<figcaption><a href="https://upload.wikimedia.org/wikipedia/commons/e/ee/World_map_of_share_of_the_population_fully_vaccinated_against_COVID-19_by_country.png">Source: Wikipedia's version of the file uploaded on 3-December-2021</a> </figcaption>
</figure>

What does the above image illustrate?

The image demonstrates how unequal the Covid-19 vaccine distribution has been across the countries. 

[Quoting the official data](https://web.archive.org/web/20211204163451/https://ourworldindata.org/covid-vaccinations) on Covid vaccines, 
> 54.9% of the world population has received at least one dose of a COVID-19 vaccine.
> Only 6.2% of people in low-income countries have received at least one dose.

(on 4th December 2021)

Why is it so?

Is there a scarcity of vaccine units, or is the vaccine being developed at a slow rate? 

No!

In fact, the vaccine production is accelerating, but [the wealthy countries are restricting the access intentionally](https://www.theguardian.com/commentisfree/2021/sep/09/west-vaccine-doses-covid-production). Also, developed countries have preordered more number of vaccines than they require and there are only a few companies developing these vaccines, which means that people in low-income developing countries [may not receive vaccinations from these manufacturers until 2023 or 2024](https://www.eiu.com/n/85-poor-countries-will-not-have-access-to-coronavirus-vaccines). 

Why this is so? This is where the profit of Pharmaceutical companies comes in and Bill Gates too.  

Basically, the covid vaccine is patented (Patent is a legal right given to someone which excludes others from making or selling an "invention" ) and pharma companies other than companies who hold the patent on the vaccine cannot produce the vaccine. 

When Oxford planned to allow all companies to manufacture its Covid vaccine, Bill Gates [convinced them to cancel this plan](https://aftinet.org.au/cms/node/1932). [Another article](https://khn.org/news/rather-than-give-away-its-covid-vaccine-oxford-makes-a-deal-with-drugmaker/). This is because Bill Gates prioritizes his profit over people's health.  

Next, Bill Gates came up with a scheme named Covax which, instead of removing the artificial restrictions from the vaccine and allowing independent companies to develop the vaccine, [plans to charge high amount of money to wealthy countries and then donate the vaccine using to poor countries](https://www.commondreams.org/news/2021/04/22/no-hiding-behind-covax-anymore-critics-say-delivery-shortfall-shows-dire-need). 

Companies participating in Covax will be able to set their own prices, without any transparency and accountability. They won't have to face any legal liability for any potential damages caused by the vaccine made by them. This is what companies want-- to earn as much as possible without any transparency and accountability of their actions.  

So, basically, what is going on is that the taxpayers of rich countries are paying for higher prices set by pharma companies so that they can donate the vaccines to low-income countries rather than allow local manufacturers in poor countries to develop their own vaccine. The vaccine distribution could have been better in low-income countries if the local manufacturers were allowed to develop the vaccine. Poor countries are doomed to suffer because of the greed of corporates. [Here is a good article](https://theconversation.com/the-big-barriers-to-global-vaccination-patent-rights-national-self-interest-and-the-wealth-gap-153443) explaining how patents are a barrier to vaccine distribution. Additionally, this scheme [would also affect rich countries' economies](https://iccwbo.org/media-wall/news-speeches/study-shows-vaccine-nationalism-could-cost-rich-countries-us4-5-trillion). 

<figure>
<img src="/images/rich-countries-vaccine-early.png" width="500" height="300">
<figcaption> Source:  <a href="https://www.eiu.com/n/rich-countries-will-get-access-to-coronavirus-vaccines-earlier-than-others/">The Economist Intelligence Unit </a></figcaption>
</figure>

In fact, executive of one vaccine manufactuer said [there would be a chance for them to raise prices for the vaccine when COVID moves from a pandemic state to an endemic situation](https://www.businessinsider.com.au/pfizer-execs-highlight-significant-opportunity-hike-covid-vaccine-price-2021-3) and the virus circulates continually in pockets around the globe. When everyone is safe, the Covid will be over. Instead the company wants the opposite, so that the virus spreads and they can earn more profit. 

The scenario is not special to Covid vaccine. Billionaires like Bill Gates have a large influence on public health policies. The root of the problem is capitalism-- profit over people or in other words, the system which incentivizes greed over everything else. It is a customary practice for rich to invent schemes like this to remain rich at the expense on others. No surprise that they wouldn't treat Covid vaccine any different.  

<br>
<b> Articles for further reading:</b>

- [How Bill Gates Impeded Global Access to Covid Vaccines](https://newrepublic.com/article/162000/bill-gates-impeded-global-access-covid-vaccines)

- [How Pfizer Silences World Governments in Vaccine Negotiations](https://www.citizen.org/news/report-how-pfizer-silences-world-governments-in-vaccine-negotiations/)

	The article mentions:

	- Pfizer silences governments through the use of nondisclosure provisions in many of its contracts. Brazil, for example, is prohibited from making “any public announcement concerning the existence… or terms” of the contract or commenting on its relationship with Pfizer without Pfizer’s prior written consent.

	- Pfizer can disallow governments from accepting additional donations of the Pfizer vaccine.

	- Pfizer exempts itself from liability for patent infringements, shifting the financial risk of Pfizer’s actions to government purchasers – despite Pfizer’s opposition to similar exemptions for manufacturers proposed at the World Trade Organization. 

	- It gives the power to secret private arbitrators, not public courts, to decide issues on contract disputes. 

	- Pfizer requires some countries to waive sovereign immunity, so it can go after state assets in case of a dispute. 

	- Pfizer gives itself sole power when it comes to making key decisions, including how vaccine deliveries will be prioritized if there is a supply shortage.

	[Another article for more information](https://www.citizen.org/article/pfizers-power/).

- [Pressuring low-income countries](https://nytimes.com/2020/11/23/world/bill-gates-vaccine-coronavirus.html).

- [Moderna not sharing its technology stopping the poorest nations to produce the shot themselves](https://nytimes.com/2021/09/22/us/politics/covid-vaccine-moderna-global.html).
