---
title: "Asante Kenya for a Good Time"
date: 2024-11-05T00:55:21+05:30
draft: false
type: "post"
tags: ["africa", "kenya", "travel"]
showTableOfContents: true
---
In September of this year, I visited Kenya to attend the [State of the Map conference](/posts/sotm-2024/). I spent six nights in the capital Nairobi, two nights in Mombasa, and one night on a train. I was very happy with the visa process being [smooth and quick](/posts/kenya-visa-process/). During the conference, I stayed at the Nairobi Transit Hotel with other attendees, with Ibtehal from Bangladesh as my roommate. I found it interesting that the shops around the hotel were grated, presumably to protect against robberies. The hotel guard had to open the lock of the hotel for us to go out at night. Further, I noticed that the banks had three layers of security. Despite this, I and Ibtehal used to hangout at a coffee shop during midnight.

{{< figure src="/images/kenya/coffee-shop.jpg" title="The coffee shop Ibtehal and me used to visit during the midnight" width="500" >}}

{{< figure src="/images/kenya/grating-at-a-chemist-shop.jpg" title="Grating at a chemist shop in Mombasa, Kenya" width="500" >}}

The country lies on the equator, which might give the impression of extremely hot temperatures. However, Nairobi was on the cooler side (10–25 degrees Celsius), and I found myself needing a hoodie, which I bought the next day. It also served as a nice souvenir, as it had an outline of the African map printed on it. 

I also bought a Safaricom SIM card for 100 shillings and recharged it with 1000 shillings for 8 GB internet with 5G speeds and 400 minutes talk time.

## A visit to Nairobi's Historic Cricket Ground

On this trip, I got a unique souvenir that can’t be purchased from the market—a cricket jersey worn in an ODI match by a player. The story goes as follows: I was roaming around the market with my friend Benson from Nairobi to buy a Kenyan cricket jersey for myself, but we couldn’t find any. So, Benson had the idea of visiting the Nairobi Gymkhana Club, which used to be Kenya’s main cricket ground. It has hosted some historic matches, including the 2003 World Cup match in which Kenya [beat](https://www.espncricinfo.com/series/icc-world-cup-2002-03-61124/kenya-vs-sri-lanka-26th-match-65258/full-scorecard) the mighty Sri Lankans and the record for the [fastest](https://www.espncricinfo.com/series/kca-centenary-tournament-1996-97-60990/pakistan-vs-sri-lanka-6th-match-66057/full-scorecard) ODI century by Shahid Afridi in just 37 balls in 1996.

Although entry to the club was exclusively for members, I was warmly welcomed by the staff. Upon reaching the cricket ground, I met some Indian players who played in Kenyan leagues, as well as [Lucas Oluoch](https://www.espncricinfo.com/cricketers/lucas-oluoch-440162) and [Dominic Wesonga](https://www.espncricinfo.com/cricketers/dominic-wesonga-315051), who have represented Kenya in ODIs. When I expressed interest in getting a jersey, Dominic agreed to send me pictures of his jersey. I liked his jersey and collected it from him. I gave him 2000 shillings, an amount suggested by those Indian players.

{{< figure src="/images/kenya/players-at-ground.jpg" title="Me with players at the Nairobi Gymkhana Club" width="500" >}}

{{< figure src="/images/kenya/cricket-pitch.jpg" title="Cricket pitch at the Nairobi Gymkhana Club" width="500" >}}

{{< figure src="/images/kenya/cricket-ground.jpg" title="A view of the cricket ground inside the Nairobi Gymkhana Club" width="500" >}}

{{< figure src="/images/kenya/scoreboard-at-narobi-gymkhana-club.jpg" title="Scoreboard at the Nairobi Gymkhana cricket ground" width="500" >}}

## Giraffe Center in Nairobi

Kenya is known for its safaris and has [no shortage of national parks](https://en.wikipedia.org/wiki/List_of_national_parks_of_Kenya). In fact, Nairobi is the only capital in the world with a national park. I decided not to visit one, as most of them were expensive and offered multi-day tours, and I didn’t want to spend that much time in the wildlife.

Instead, I went to the Giraffe Center in Nairobi with Pragya and Rabina. The ticket cost 1500 Kenyan shillings (1000 Indian rupees). In Kenya, matatus - shared vans, usually decorated with portraits of famous people and play rap songs - are the most popular means of public transport.  Reaching the Giraffe Center from our hotel required taking five matatus for 150 shillings and a 2 kilometer walk. The journey back was 90 shillings, suggesting that we didn’t find the most efficient route to get there. At the Giraffe Center, we fed giraffes and took photos.

{{< figure src="/images/kenya/notorious-big.jpg" title="A matatu with a Notorious BIG portrait." width="500" >}}

{{< figure src="/images/kenya/inside-the-giraffe-center.jpg" title="Inside the Giraffe Center" width="500" >}}

## Train ride from Nairobi to Mombasa

I wanted to visit a place outside of Nairobi. Mombasa being a coastal city and the second-largest in Kenya was a natural choice. It is 500 kilometers from the capital Nairobi. I love trains in general, so I decided to take the SGR train from Nairobi to Mombasa. I tried reserving a seat for myself from home, but found out that the train could only be booked using M-PESA. It is a mobile bank transfer system in Kenya, and I didn’t have an M-PESA account. Therefore, I could not book my ticket in advance. When I was in Kenya, I was helped by Pragya’s friend Mary for booking my ticket. It was a second-class ticket for 1500 shillings (1000 Indian rupees). 

If you are a tourist, note that local shops in Kenya can facilitate such an M-PESA transfer in exchange for cash. Your hotel may also be providing such a service, so make sure to check with them.

The train was scheduled to depart from Nairobi at 08:00 hours in the morning and arrive in Mombasa at 14:00 hours. The security check at the station required scanning bags and having them sniffed by sniffer dogs. I also fell victim to a scam by a security official who offered to help me get my ticket printed, only to later ask me to get him some coffee, which I politely declined.

Before boarding the train, I was treated to some stunning views at the Nairobi Terminus station. It was a seating train, but I wished it were a sleeper train, as I was sleep-deprived. The train was neat and clean, with good toilets. It reached Mombasa on time.

{{< figure src="/images/kenya/nairobi-terminus.jpg" title="SGR train at Nairobi Terminus." width="500" >}}

{{< figure src="/images/kenya/sgr-interior.jpg" title="Interior of the SGR train" width="500" >}}

## Arrival in Mombasa

{{< figure src="/images/kenya/mombasa-terminus.jpg" title="Mombasa Terminus station." width="500" >}}

Mombasa was a bit hotter than Nairobi, with temperatures reaching around 30 degrees Celsius. However, that’s not too hot for me, as I am used to higher temperatures in India. I had booked a hostel in the Old Town and was searching for a hitchhike from the Mombasa Terminus station. After trying for more than half an hour, I took a matatu that dropped me 3 km from my hostel for 200 shillings (140 Indian rupees). I tried to hitchhike again but couldn’t find a ride.

I think I know why I couldn't get a ride in both the cases. In the first case, the Mombasa Terminus was in an isolated place, so most of the vehicles were taxis or matatus while any noncommercial cars were there to pick up friends and family. If the station were in the middle of the city, there would be many more car/truck drivers passing by, thus increasing my possibilities of getting a ride. In the second case, my hostel was at the end of the city, and nobody was going towards that side. In fact, many drivers told me they would love to give me a ride, but they were going in some other direction.

Finally, I took a tuktuk for 70 shillings to reach my hostel, Tulia Backpackers. It was 11 USD (1400 shillings) for one night. The balcony gave a nice view of the Indian Ocean. The rooms had fans, but there was no air conditioning. Each bed also had mosquito nets. The place was walking distance of the famous [Fort Jesus](https://en.wikipedia.org/wiki/Fort_Jesus). Mombasa has had more Islamic influence compared to Nairobi and also has many Hindu temples.

{{< figure src="/images/kenya/balcony-at-tulia.jpg" title="The balcony at Tulia Backpackers Hostel had a nice view of the ocean." width="500" >}}

{{< figure src="/images/kenya/room-in-tulia.jpg" title="A room inside the hostel with fans and mosquito nets on the beds" width="500" >}}

## Visiting White Sandy Beaches and Getting a Hitchhike

Visiting Nyali beach marked my first time ever at a white sand beach. It was like 10 km from the hostel. The next day, I visited Diani Beach, which was 30 km from the hostel. Going to Diani Beach required crossing a river, for which there's a free ferry service every few minutes, followed by taking a matatu to Ukunda and then a tuk-tuk. The journey gave me a glimpse of the beautiful countryside of Kenya.

{{< figure src="/images/kenya/nyali-beach.jpg" title="Nyali beach is a white sand beach" width="500" >}}

{{< figure src="/images/kenya/ferry.jpg" title="This is the ferry service for crossing the river." width="500" >}}

During my return from Diani Beach to the hostel, I was successful in hitchhiking. However, it was only a 4 km ride and not sufficient to reach Ukunda, so I tried to get another ride. When a truck stopped for me, I asked for a ride to Ukunda. Later, I learned that they were going in the same direction as me, so I got off within walking distance from my hostel. The ride was around 30 km. I also learned the difference between a truck ride and a matatu or car ride. For instance, matatus and cars are much faster and cooler due to air conditioning, while trucks tend to be warmer because they lack it. Further, the truck was stopped at many checkpoints by the police for inspections as it carried goods, which is not the case with matatus. Anyways, it was a nice experience, and I am grateful for the ride. I had a nice conversation with the truck drivers about Indian movies and my experiences in Kenya.

{{< figure src="/images/kenya/diani-beach.jpg" title="Diani beach is a popular beach in Kenya. It is a white sand beach." width="500" >}}

{{< figure src="/images/kenya/selfie-with-truck-drivers.jpg" title="Selfie with truck drivers who gave me the free ride" width="500" >}}

## Back to Nairobi

I took the SGR train from Mombasa back to Nairobi. This time I took the night train, which departs at 22:00 hours, reaching Nairobi at around 04:00 in the morning. I could not sleep comfortably since the train only had seater seats. 

I had booked the Zarita Hotel in Nairobi and  had already confirmed if they allowed early morning check-in. Usually, hotels have a fixed checkout time, say 11:00 in the morning, and you are not allowed to stay beyond that regardless of the time you checked in. But this hotel checked me in for 24 hours. Here, I paid in US dollars, and the cost was 12 USD.

## Almost Got Stuck in Kenya

Two days before my scheduled flight from Nairobi back to India, I heard the news that the airports in Kenya were [closed](https://www.bbc.co.uk/news/articles/c3rdg13z1j7o) due to the strikes. Rabina and Pragya had their flight back to Nepal canceled that day, which left them stuck in Nairobi for two additional days. I called Sahil in India and found out during the conversation that the strike was [called off](https://www.reuters.com/world/africa/strike-kenyas-main-airport-causing-flight-delays-cancellations-kenya-airways-2024-09-11/) in the evening. It was a big relief for me, and I was fortunate to be able to fly back to India without any changes to my plans.

{{< figure src="/images/kenya/newspapers.jpg" title="Newspapers at a stand in Kenya covering news on the airport closure" width="500" >}}

## Experience with locals

I had no problems communicating with Kenyans, as everyone I met knew English to an extent that could easily surpass that of big cities in India. Additionally, I learned a few words from Kenya’s most popular local language, Swahili, such as “Asante,” meaning “thank you,” “Jambo” for “hello,” and “Karibu” for “welcome.” Knowing a few words in the local language went a long way.

I am not sure what's up with haggling in Kenya. It wasn't easy to bring the price of souvenirs down. I bought a fridge magnet for 200 shillings, which was the quoted price. On the other hand, it was much easier to bargain with taxis/tuktuks/motorbikes.

I stayed at three hotels/hostels in Kenya. None of them had air conditioners. Two of the places were in Nairobi, and they didn’t even have fans in the rooms, while the one in Mombasa had only fans. All of them had good Wi-Fi, except Tulia where the internet overall was a bit shaky.

My experience with the hotel staff was great. For instance, we requested that the Nairobi Transit Hotel cancel the included breakfast in order to reduce the room costs, but later realized that it was not a good idea. The hotel allowed us to revert and even offered one of our missing breakfasts during dinner. 

The staff at Tulia Backpackers in Mombasa facilitated the ticket payment for my train from Mombasa to Nairobi. One of the staff members also gave me a lift to the place where I could catch a matatu to Nyali Beach. They even added an extra tea bag to my tea when I requested it to be stronger.

## Food

At the Nairobi Transit Hotel, a Spanish omelet with tea was served for breakfast. I noticed that Spanish omelette appeared on the menus of many restaurants, suggesting that it is popular in Kenya. This was my first time having this dish. The milk tea in Kenya, referred to by locals as “white tea,” is lighter than Indian tea (they don't put a lot of tea leaves). 

{{< figure src="/images/kenya/spanish-omlette.jpg" title="Spanish Omelette served in breakfast at Nairobi Transit Hotel" width="500" >}}

I also sampled ugali with eggs. In Mombasa, I visited an Indian restaurant called New Chetna and had a buffet thali there twice.

{{< figure src="/images/kenya/ugali-with-eggs.jpg" title="Ugali with eggs." width="500" >}}

## Tips for Exchanging Money 

In Kenya, I exchanged my money at forex shops a couple of times. I received good exchange rates for bills larger than 50 USD. For instance, 1 USD on xe.com was 129 shillings, and I got 128.3 shillings per USD (a total of 12,830 shillings) for two 50 USD notes at an exchange in Nairobi, while 127 shillings, which was the highest rate at the banks. On the other hand, for smaller bills such as a one US dollar note, I would have got 125 shillings. A passport was the only document required for the exchange, and they also provided a receipt.

My advice for travelers would be to keep 50 USD or larger bills for exchanging into the local currency while saving the smaller US dollar bills for accommodation, as many hotels and hostels accept payment in US dollars (in addition to Kenyan shillings).

## Missed Malindi and Lamu

There were more places on my to-visit list in Kenya. But I simply didn't have time to cover them, as I don't like rushing through places, especially in a foreign country where there is a chance of me underestimating the amount of time it takes during transit. I would have liked to visit at least one of [Kilifi](https://en.wikivoyage.org/wiki/Kilifi), [Watamu](https://en.wikivoyage.org/wiki/Malindi#Watamu_beaches) or [Malindi](https://en.wikivoyage.org/wiki/Malindi) beaches. Further, [Lamu](https://en.wikivoyage.org/wiki/Lamu) seemed like a unique place to visit as it has no cars or motorized transport; the only options for transport are boats and donkeys. But I missed Lamu as well.

That's it for now. Meet you in the next one :)
