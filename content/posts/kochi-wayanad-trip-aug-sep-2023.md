---
title: "Kochi - Wayanad Trip in August-September 2023"
date: 2023-10-14T17:48:54+05:30
draft: false
type: "post"
showTableOfContents: true
tags: ["travel", "Kerala", "Kochi", "Wayanad", "trekking","hitchhiking", "india"]
---
**A trip full of hitchhiking, beautiful places and welcoming locals.**

## Day 1: Arrival in Kochi

Kochi is a city in the state of Kerala, India. This year’s [DebConf](https://debconf23.debconf.org/) was to be held in Kochi from 3rd September to 17th of September, which I was planning to attend. My friend Suresh, who was planning to join, told me that 29th August 2023 will be Onam, a major festival of the state of Kerala. So, we planned a Kerala trip before the DebConf. We booked early morning flights for Kochi from Delhi and reached Kochi on 28th August.

We had booked a hostel named Zostel in Ernakulam. During check-in, they asked me to fill a form which required signing in using a Google account. I told them I don’t have a Google account and I don’t want to create one either. The people at the front desk seemed receptive, so I went ahead with telling them the problems of such a sign-in being mandatory for check-in. Anyways, they only took a photo of my passport and let me check-in without a Google account.

We stayed in a ten room dormitory, which allowed travellers of any gender. The dormitory room was air-conditioned, spacious, clean and beds were also comfortable. There were two bathrooms in the dormitory and they were clean. Plus, there was a separate dormitory room in the hostel exclusive for females. I noticed that that Zostel was not added in the OpenStreetMap and so, I added it :) . The hostel had a small canteen for tea and snacks, a common sitting area outside the dormitories, which had beds too. There was a separate silent room, suitable for people who want to work.

{{< figure loading="lazy" src="/images/kochi-wayanad-aug-sep-2023/zostel-room-1.jpg" title="Dormitory room in Zostel Ernakulam, Kochi." >}}

{{< figure loading="lazy" src="/images/kochi-wayanad-aug-sep-2023/zostel-room-2.jpg" title="Beds in Zostel Ernakulam, Kochi." >}}

We had lunch at a nearby restaurant and it was hard to find anything vegetarian for me. I bought some freshly made banana chips from the street and they were tasty. As far as I remember, I had a big glass of pineapple juice for lunch. Then I went to the Broadway market and bought some cardamom and cinnamon for home. I also went to a nearby supermarket and bought Matta brown rice for home. Then, I looked for a courier shop to send the things home but all of them were closed due to Onam festival. After returning to the Zostel, I overslept till 9 PM and in the meanwhile, Suresh planned with Saidut and Shwetank (who met us during our stay in Zostel) to go to a place in Fort Kochi for dinner. I suspected I will be disappointed by lack of vegetarian options as they were planning to have fish. I already had a restaurant in mind - Brindhavan restaurant (suggested by Anupa), which was a pure vegetarian restaurant.

To reach there, I got off at Palarivattom metro station and started looking for an auto-rickshaw to get to the restaurant. I didn’t get any for more than 5 minutes. Since that restaurant was not added to the OpenStreetMap, I didn’t even know how far that was and which direction to go to. Then, I saw a Zomato delivery person on a motorcycle and asked him where the restaurant was. It was already 10 PM and the restaurant closes at 10:30. So, I asked him whether he can drop me off. He agreed and dropped me off at that restaurant. It was 4-5 km from that metro station. I tipped him and expressed my gratefulness for the help. He refused to take the tip, but I insisted and he accepted.

I entered the restaurant and it was coming to a close, so many items were not available. I ordered some Kadhai Paneer (only item left) with naan. It tasted fine. Since the next day was Thiruvonam, I asked the restaurant about the Sadya thali menu and prices for the next day. I planned to eat Sadya thali at that restaurant, but my plans got changed later.

{{< figure loading="lazy" src="/images/kochi-wayanad-aug-sep-2023/onam-sadya-menu.jpg" title="Onam sadya menu from Brindhavan restaurant." width="400">}}

## Day 2: Onam celebrations

Next day, on 29th of August 2023, we had plan to leave for Wayanad. Wayanad is a hill station in Kerala and a famous tourist spot. Praveen suggested to visit Munnar as it is far closer to Kochi than Wayanad (80 km vs 250 km). But I had already visited Munnar in my previous trips, so we chose Wayanad. We had a train late night from Ernakulam Junction (at 23:30 hours) to Kozhikode, which is the nearest railway station from Wayanad. So, we checked out in the morning as we had plans to roam around in Kochi before taking the train.

Zostel was celebrating Onam on that day. To opt-in, we had to pay 400 rupees, which included a [Sadya Thali](https://en.wikipedia.org/wiki/Sadya) and a [Mundu](https://en.wikipedia.org/wiki/Mundu). Me and Suresh paid the amount and opted in for the celebrations. Sadya thali had Rice, Sambhar, Rasam, Avial, Banana Chips, Pineapple Pachadi, Pappadam, many types of pickels and chutneys, Pal Ada Payasam and Coconut jaggery Pasam. And, there was water too :). Those payasams were really great and I had one more round of them. Later, I had a lot of variety of payasams during the DebConf.

{{< figure loading="lazy" src="/images/kochi-wayanad-aug-sep-2023/sadya-thalis.jpg" title="Sadya lined up for serving" width="300" >}}

{{< figure loading="lazy" src="/images/kochi-wayanad-aug-sep-2023/sadya.jpg" title="Sadya thali served on banana leaf." >}}

In the evening, we hung out in the common room and put our luggage there. We played UNO and had conversations with other travellers in the hostel. I had a fun time there and I still think it is one of the best hostel experiences I had. We made good friends with Saiduth (Telangana) and Shwetank (Uttarakhand). They were already aware about the software like debian, and we had some detailed conversations about the Free Software movement. I remember explaining the [difference between the terms “Open Source” and “Free Software”](/posts/fs-and-oss-same). I also told them about the Streetcomplete app, a beginner friendly app to edit OpenStreetMap. We had dinner at a place nearby (named Palaraam), but again, the vegetarian options were very limited! After dinner, we came back to the Zostel and me and Suresh left for Ernakulam Junction to catch our train Maveli Express (16604).

## Day 3: Going to Wayanad

Maveli Express was scheduled to reach Kozhikode at 03:25 (morning). I had set alarms from 03:00 to 03:30, with the gap of 10 minutes. Every time I woke up, I turned off the alarm. Then I woke up and saw train reaching the Kozhikode station and woke up Suresh for deboarding. But then I noticed that the train is actually leaving the station, not arriving! This means we missed our stop. Now we looked at the next stops and whether we can deboard there. I was very sleepy and wanted to take a retiring room at some station before continuing our journey to Wayanad. The next stop was Quilandi and we checked online that it didn’t have a retiring room. So, we skipped this stop. We got off at the next stop named Vadakara and found out no retiring room was available. So, we asked about information regarding bus for Wayanad and they said that there is a bus to Wayanad around 07:00 hours from bus station which was a few kilometres from the railway station.

We took a bus for Kalpetta (in Wayanad) at around 07:00. The destination of the buses were written in Malayalam, which we could not read. Once again, the locals helped us to get on to the bus to Kalpetta. Vadakara is not a big city and it can be hard to find people who know good Hindi or English, unlike Kochi. Despite language issues, I had no problem there in navigation, thanks to locals. I mostly spent time sleeping during the bus journey.

A few hours later, the bus dropped us at Kalpetta. We had a booking at a hostel in Rippon village. It was 16 km from Kalpetta. On the way, we were treated with beautiful views of nature, which was present everywhere in Wayanad. The place was covered with tea gardens and our eyes were treated with beautiful scenery at every corner.

{{< figure loading="lazy" src="/images/kochi-wayanad-aug-sep-2023/scenery-in-wayanad.jpg" title="We were treated with such views during the Wayanad trip." >}}

Rippon village was a very quiet place and I liked the calm atmosphere. This place is blessed by nature and has stunning scenery. I found English was more common than Hindi in Wayanad. Locals were very nice and helpful, even if they didn’t know my language.

{{< figure loading="lazy" src="/images/kochi-wayanad-aug-sep-2023/rippon.jpg" title="A road in Rippon." >}}

After catching some sleep at the hostel, I went out in the afternoon. I hitchhiked to reach the main road from the hostel. I bought more spices from a nearby shop and realized that I should have waited for my visit to Wayanad to buy cardamom, which I already bought from Kochi. Then, I was looking for post office to send spices home. The people at the spices shop told me that the nearby Rippon post office was closed by that time, but the post office at Meppadi was open, which was 5 km from there.

I went to Meppadi and saw the post office closes at 15:00, but I reached five minutes late. My packing was not very good and they asked me to pack it tighter. There was a shop near the post office and the people there gave me a cardboard and tapes, and helped pack my stuff for the post. By the time I went to the post office again, it was 15:30. But they accepted my parcel for post.

## Day 4: Kanthanpara Falls, Zostel Wayanad and Karapuzha Dam

Kanthanpara waterfalls were 2 km from the hostel. I hitchhiked to the place from the hostel on a scooty. Entry ticket was worth Rs 40. There were good views inside and nothing much to see except the waterfalls.

{{< figure loading="lazy" src="/images/kochi-wayanad-aug-sep-2023/kanthanpara-1.jpg" title="Entry to Kanthanpara Falls." >}}

{{< figure src="/images/kochi-wayanad-aug-sep-2023/kanthanpara-2.jpg" title="Kanthanpara Falls." >}}

We had a booking at Zostel Wayanad for this day and so we shifted there. Again, as with their Ernakulam branch, they asked me to fill a form which required signing in using Google, but when I said I don’t have a Google account they checked me in without that. There were tea gardens inside the Zostel boundaries and the property was beautiful.

{{< figure loading="lazy" src="/images/kochi-wayanad-aug-sep-2023/zostel-wayanad-1.jpg" title="A view of Zostel Wayanad." >}}

{{< figure loading="lazy" src="/images/kochi-wayanad-aug-sep-2023/zostel-wayanad-2.jpg" title="A map of Wayanad showing tourist places." >}}

{{< figure loading="lazy" src="/images/kochi-wayanad-aug-sep-2023/zostel-wayanad-3.jpg" title="A view from inside the Zostel Wayanad property." >}}

Later in the evening, I went to Karapuzha Dam. I witnessed a beautiful sunset during the journey. Karapuzha dam had many activites, like ziplining, and was nice to roam around.

Chembra Peak is near to the Zostel Wayanad. So, I was planning to trek to the heart shaped lake. It was suggested by Praveen and looking online, this trek seemed worth doing. There was an issue however. The charges for trek were Rs 1770 for upto five people. So, if I go alone I will have to spend Rs 1770 for the trek. If I go with another person, we split Rs 1770 into two, and so on. The optimal way to do it is to go in a group of five (you included :D). I asked front desk at Zostel if they can connect me with people going to Chembra peak the next day, and they told me about a group of four people planning to go to Chembra peak the next day. I got lucky! All four of them were from Kerala and worked in Qatar.

## Day 5: Chembra peak trek

The date was 1st September 2023. I woke up early (05:30 in the morning) for the Chembra peak trek. I had bought hiking shoes especially for trekking, which turned out to be a very good idea. The ticket counter opens at 07:00. The group of four with which I planned to trek met me around 06:00 in the Zostel. We went to the ticket counter around 06:30. We had breakfast at shops selling Maggi noodles and bread omlette near the ticket counter.

It was a hot day and the trek was difficult for an inexperienced person like me. The scenery was green and beautiful throughout.

The day was scorching hot, and the trek proved to be challenging for someone like me, who was relatively inexperienced. However, the lush green scenery surrounding us was incredibly beautiful throughout the journey.

{{< figure loading="lazy" src="/images/kochi-wayanad-aug-sep-2023/chembra-1.jpg" title="Terrain during trekking towards the Chembra peak." >}}

{{< figure loading="lazy" src="/images/kochi-wayanad-aug-sep-2023/chembra-2.jpg" title="Heart-shaped lake at the Chembra peak." >}}

{{< figure loading="lazy" src="/images/kochi-wayanad-aug-sep-2023/chembra-3.jpg" title="Me at the heart-shaped lake." width="500" >}}

{{< figure loading="lazy" src="/images/kochi-wayanad-aug-sep-2023/chembra-4.jpg" title="Views from the top of the Chembra peak." >}}

{{< figure loading="lazy" src="/images/kochi-wayanad-aug-sep-2023/chembra-5.jpg" title="View of another peak from the heart-shaped lake." >}}

On the way back from the trek, I stumbled upon a shop selling bamboo rice, which I purchased with the intention of making bamboo rice payasam at home (I even have some coconut milk from Kerala to use ;)). We arrived back at Zostel in the afternoon. I experienced muscle soreness after the trek, and it has yet to completely subside. Later that night, we boarded a bus from Kalpetta to Kozhikode to begin our journey back to Kochi.

## Day 6: Return to Kochi 

At midnight on the 2nd of September, we arrived at Kozhikode bus stand. Afterward, we wandered around in search of something to eat, but unfortunately, I couldn’t find any vegetarian options. Not a surprising turn of events, considering Kozhikode is especially famous for its non-vegetarian dishes. We then headed to Kozhikode railway station to inquire about retiring rooms, but we were out of luck. We waited at the station and caught the next train to Kochi at 03:30, arriving at Ernakulam Junction at 07:30, half an hour before the scheduled arrival time of the train. From there, we made our way to Zostel Fort Kochi, where we spent the night before checking out the next morning.

## Day 7: Roaming around in Fort Kochi

On the 3rd of September, we explored Fort Kochi, visiting popular landmarks such as St. Francis Church, Dutch Palace, Jew Town, and the Pardesi Synagogue. During our visit, I also had the opportunity to tour some homestays, where the owners graciously showed me around their properties, despite my clear indication that I was not seeking accommodation. In the evening, we made our way to Kakkanad to participate in DebConf.

For more details on my experiences at DebConf23, you can continue reading in [my DebConf23 blog post](/posts/debconf23).
