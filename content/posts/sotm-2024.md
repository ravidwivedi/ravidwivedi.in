---
title: "State of the Map Conference in Kenya"
date: 2024-10-01T19:35:30+05:30
draft: false
type: "post"
tags: ["conference", "kenya", "africa", "openstreetmap"]
showTableOfContents: true
---
Last month, I traveled to Kenya to attend a conference called [State of the Map 2024](https://2024.stateofthemap.org/) ("SotM" for short), which is an annual meetup of [OpenStreetMap](https://wiki.openstreetmap.org/wiki/About_OpenStreetMap) contributors from all over the world. It was held at the University of Nairobi Towers in Nairobi, from the 6th to the 8th of September.

{{< figure src="/images/sotm-2024/university-of-nairobi.jpg" title="University of Nairobi." width="700" loading="lazy" >}}

I have been contributing to OpenStreetMap for the last three years, and this conference seemed like a great opportunity to network with others in the community. As soon as I came across the travel grant announcement, I jumped in and filled the form immediately. I was elated when I was selected for the grant and couldn't wait to attend. The grant had an upper limit of €1200 and covered food, accommodation, travel and miscellaneous expenses such as visa fee.

Pre-travel tasks included obtaining Kenya's eTA and getting a yellow fever vaccine. Before the conference, Mikko from the [Humanitarian OpenStreetMap Team](https://www.hotosm.org/) introduced me to Rabina and Pragya from Nepal, Ibtehal from Bangladesh, and Sajeevini from Sri Lanka. We all booked the Nairobi Transit Hotel, which was within walking distance of the conference venue. Pragya, Rabina, and I traveled together from Delhi to Nairobi, while Ibtehal was my roommate in the hotel.

{{< figure src="/images/sotm-2024/south-asian-group.jpg" title="Our group at the conference." width="700" loading="lazy">}}

The venue, University of Nairobi Towers, was a tall building and the conference was held on the fourth, fifth and sixth floors. The open area on the fifth floor of the building had a nice view of Nairobi's skyline and was a perfect spot for taking pictures. Interestingly, the university had a wing dedicated to [Mahatma Gandhi](https://en.wikipedia.org/wiki/Mahatma_Gandhi), who is regarded in India as the Father of the Nation.

{{< figure src="/images/sotm-2024/nairobi-skyline.jpg" title="View of Nairobi's skyline from the open area on the fifth floor." width="700" loading="lazy" >}}

{{< figure src="/images/sotm-2024/gandhi-library.jpg" title="A library in Mahatma Gandhi wing of the University of Nairobi." width="700" loading="lazy" >}}

The diversity of the participants was mind-blowing, with people coming from a whopping 54 countries. I was surprised to notice that I was the only participant traveling from India, despite India having a large OpenStreetMap community. That said, there were two other Indian participants who traveled from other countries. I finally got to meet Arnalie (from the Phillipines) and Letwin (from Zimbabwe), both of whom I had only met online before. I had met Anisa (from Albania) earlier during [DebConf 2023](/posts/debconf23). But I missed Mikko and Honey from the Humanitarian OpenStreetMap Team, whom I knew from the Open Mapping Guru program.

I learned about the extent of OSM use through Pragya and Rabina's talk; about the logistics of running the OSM Board, in the OSMF (OpenStreetMap Foundation) session; about the Youth Mappers from Sajeevini, about the OSM activities in Malawi from Priscilla Kapolo, and about mapping in Zimbabwe from Letwin. However, I missed Ibtehal's lightning session. The ratio of women speakers and participants at the conference was impressive, and I hope we can get such gender representation in our Delhi/NCR mapping parties.

{{< figure src="/images/sotm-2024/conference-hall.jpg" title="One of the conference halls where talks took place." width="700" loading="lazy" >}}

Outside of talks, the conference also had lunch and snack breaks, giving ample time for networking with others. In the food department, there were many options for a lacto-ovo vegetarian like myself, including potatoes, rice, beans, chips etc. I found out that the milk tea in Kenya (referred to as "white tea") is usually not as strong compared to India, so I switched to coffee (which is also called "white coffee" when taken with milk). The food wasn't spicy, but I can't complain :) Fruit juices served as a nice addition to lunch.

{{< figure src="/images/sotm-2024/lunch.jpg" title="One of the lunch meals served during the conference." width="700" loading="lazy">}}

At the end of the second day of the conference, there was a surprise in store for us — a bus ride to the Bao Box restaurant. The ride gave us the experience of a typical Kenyan *matatu* (privately-owned minibuses used as share taxis), complete with loud rap music. I remember one of the songs being Kraff's Nursery Rhymes. That day, I was wearing an original Kenyan cricket jersey - one that belonged to [Dominic Wesonga](https://en.wikipedia.org/wiki/Dominic_Wesonga), who represented Kenya in four [ODIs](https://en.wikipedia.org/wiki/One_Day_International). This confused Priscilla Kapolo, who asked if I was from Kenya! Anyway, while it served as a good conversation starter, it didn't attract as much attention as I expected :) I had some pizza and chips there, and later some drinks with Ibtehal. After the party, Piyush went with us to our hotel and we played a few games of UNO.

{{< figure src="/images/sotm-2024/minibus-from-outside.jpg" title="Minibus which took us from the university to Bao Box restaurant." width="700" loading="lazy">}}

{{< figure src="/images/sotm-2024/minibus-from-inside.jpg" title="This minibus in the picture gave a sense of a real matatu." width="700" loading="lazy" >}}

I am grateful to the organizers Laura and Dorothea for introducing me to Nikhil when I was searching for a companion for my post-conference trip. Nikhil was one of the aforementioned Indian participants, and a wildlife lover. We had some nice conversations; he wanted to go to the Masai Maara Natural Reserve, but it was too expensive for me. In addition, all the safaris were multi-day affairs, and I wasn't keen on being around wildlife for that long. Eventually I chose to go my own way, exploring the coastal side and visiting Mombasa.

While most of the work regarding the conference was done using [free software](https://fsfe.org/freesoftware/index.en.html) (including the reimbursement form and Mastodon announcements), I was disappointed by the use of WhatsApp for coordination with the participants. I don't use WhatsApp and so was left out. WhatsApp is [proprietary software](https://fsf.org.in/article/better-than-whatsapp) (they do not provide the source code) and [users don't control it](https://seirdy.one/posts/2021/01/27/whatsapp-and-the-domestication-of-users/). It is common to [highlight](https://wiki.openstreetmap.org/wiki/FAQ#Why_don't_you_just_use_Google_Maps/whoever_for_your_data?) that OpenStreetMap is controlled by users and the community, rather than a company - this should apply to WhatsApp as well.

My suggestion is to use [XMPP](https://joinjabber.org), which shares similar principles with OpenStreetMap, as it is privacy-respecting, controlled by users, and powered by free software. I understand the concern that there might not be many participants using XMPP already. Although it is a good idea to onboard people to free software like XMPP, we can also create a Matrix group, and bridge it with both the XMPP group and the Telegram group. In fact, using Matrix and bridging it with Telegram is how I communicated with the South Asian participants. While it's not ideal - as Telegram's servers are proprietary and centralized - but it's certainly much better than creating a WhatsApp-only group. The setup can be bridged with IRC as well. On the other hand, self-hosted mailing lists for participants is also a good idea.

Finally, I would like to thank SotM for the generous grant, enabling me to attend this conference, meet the diverse community behind OSM and visit the beautiful country of Kenya. Stay tuned for the blog post on Kenya trip.

**Thanks to [Sahilister](https://sahilister.in), [Contrapunctus](https://contrapunctus.codeberg.page), [Snehal](https://inferred.in) and [Badri](https://badrihippo.thekambattu.rocks/) for reviewing the draft of this blog post before publishing.**
