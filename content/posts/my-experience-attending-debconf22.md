---
title: "My Experience of attending DebConf22 in Kosovo"
date: 2022-07-25T22:22:10+02:00
author: Ravi Dwivedi
tags: ["debconf", "debian", "debconf22", "kosovo", "travel"]
showTableOfContents: true
draft: false
type: "post"
---
I just came out of what has been one of the most wonderful experiences of my life– DebConf in Kosovo. DebConf is the annual conference of Debian contributors from all around the world. This was my first time attending a DebConf ever, which presented a great opportunity for me to explore different cultures and food, along with making many new friends in the Debian community as it [featured 210 attendees from 38 countries](https://www.debian.org/News/2022/20220724).

<figure>
<img src="https://wiki.debian.org/DebConf/22/Photos?action=AttachFile&do=view&target=debconf23_group_photo.jpg" width="700" >
<figcaption>DebConf 22 group photo.</figcaption>
</figure>

I registered and applied for bursary which was accepted by the DebConf bursary team. I applied for the Kosovo visa and after a long frustrating process of getting the visa, reached the venue of DebConf on 10th July 2022, which I have already written in detail. Getting Kosovo visa was already a great feat before DebConf and I am highly grateful to the organizers for putting their sincere efforts. Without this, it would not have been possible for us Indians to attend the event.

### The Venue

The DebConf venue was Innovation and Training Park in Prizren, Kosovo. As soon as we reached the ITP campus, our eyes were treated with beautiful views of mountains and blue sky in all directions. The natural beauty of the place was stunning. You take a random photo of this place and it will come off as beautiful. This is how good it was. The fact that the campus was a German military base till December 2019 spiced up many people’s interests in the history of the place and there were some discussions regarding this.

The city of Prizren has a bridge over the Lumbardhi river which also features in the DebConf 22 logo. The bridge is a must visit place in Prizren, in my opinion. There was also a castle, which had views of the beautiful city of Prizren, but I missed visiting it.

The DebCamp held from 10 July to 16 July 2022 and the DebConf happened from 17th-24th July 2022. Most of the people came at the time of DebConf and the DebCamp days did not had so many attendees. By the time DebConf started, I already got adjusted to the campus.

### Food 

Being a non-meat eater, the first thing I was worried about before arriving at the venue was food as I read on the internet that Kosovo’s food is heavily based on meat. In my first lunch, I had only Veg Pasta multiple plates as most of the items in this lunch was meat-based. In the next two-three days, the availability of vegetarian food got better and I started enjoying the food. They started serving bean soup, vegetable soup, lentils and rice in the lunch time, expanding their vegetarian dishes. I enjoyed the food and it was way better than what I expected. The coconut and chocolate pastry served in the dinner time were delicious, and there was another delicious desert called Al-Baklava. Watermelon, Apples and Musk Melon, peach were frequently served fruits. The food got repetitive though after a point and it wasn’t as enjoyable, but I think it was still good as I am not sure if there is a lot of variety to expect from Kosovo in the vegetarian section.

The availability of drinking water was through the plastic bottles in the restaurant. In my opinion, this wastes a lot of plastic and is not an environment friendly option. Probably the locals were saying that the tap water is drinkable but it would still made sense to have a water filter in the dorms or water coolers etc.

### DebConf 

When the DebConf started on 17th July, new attendees started arriving in bigger numbers. Here I made many friends in the hacklab or when sitting over the lunch. As far as I remember, the most discussed topic was politics. They were very enthusiastic about telling their country’s history and the politics and asking me about the Indian culture, food etc.

The people were very very nice and I had a great time with the Debian community. The people were from very diverse backgrounds with each one having their unique stories on their involvement with Debian. On the other hand, there were people new to Debian and they asked my viewpoints on it, which gave me opportunities to propagate the Free Software Philosophy.

The participants ranged from being Google or Canonical employees to college students and high school students. I met a lot of students from Pristina, the capital of Kosovo, for whom the DebConf must have been a good opportunity. Since Debian is for everyone, there were many people from the non-technical side of Debian. I liked the fact that the attendees included people who didn’t know English, which gave more diversity.

I attended the talks whenever the topics seemed interesting to me. Since there were three tracks of talks happening simultaneously, I missed some of them. The dining, hacklab and the the accommodation were a bit far from each other. Further, it was mountaineous area and was not a easy breeze for me to get around in the campus so quickly.

There was a noisy hacklab and a silent hacklab. I used to hangout at the noisy hacklab as that gave me chance to meet new people. The noisy hacklab had free of charge coffee, while beer and water were given on the paid basis. Attendees could volunteer to be a bartender brewing and serving beer and coffee in the hacklab. The card games I played in the hacklab were very fun. Karl from the video team told me a long story on how much travelling video team has to do with the equipment and how hectic it is. I was totally ignorant about this aspect of DebConf before Karl told me this.

I didn’t spend a lot of time in my accommodation. I used to go there mainly for sleep and shower. The walls were thin, so there was an actual email sent in some debconf mailing list about not to talk loudly in the dorms. The rooms were spacious and had good air-conditioning. By the time DebConf started, dorms had a very good Wi-Fi connection.

I volunteered for some tasks for the video team. I was a talk meister in one talk– person who introduces the speaker and gives mic to people in audience who want to ask questions from the speaker, a camera operator in two of the talks.

### Cheese and Wine Party

Cheese and Wine Party is a tradition of DebConf in which people bring food from their local areas to share with others. I brought Dal Samosa, which is a snack from my local place in India. This was, in general, liked by people in the party. I tried French alcoholic drink Pastis, which tasted like Fennel, and found it good. Cheese and Wine Party happened after the dinner, so I didn’t eat a lot. I had great conversations in the party with many people and it was fun.

<figure>
<img src="https://upload.wikimedia.org/wikipedia/commons/4/42/Dal_Samosa.jpg" width="500">
<figcaption>Dal Samosa at a shop in India. 
Credits: Ravi Dwivedi, <a href="https://creativecommons.org/licenses/by-sa/4.0">CC BY-SA 4.0</a>
</figcaption>
</figure>

### DayTrip

The DayTrip was conducted by the DebConf to take time out of their schedule to visit some places in Kosovo. You can see all the options of DayTrips [here](https://wiki.debian.org/DebConf/22/DayTrip/).

I went to tour A: Bus tour of Eastern Kosovo. It covered Gadime Cave, Ulpiana Ancient City, Gracanica Monastery, Bear Sanctuary, Artana (Novoberda) Fortress. My feeling was that the day trip was trying to cover a lot of places, so it became more rushed. The places were beautiful and worth visiting in Kosovo. The tour featured an unexpected hiking in the Bear Sanctuary. The weather was sunny and very hot, so overall it was a tiring trip with beautiful places and good pictures.

The tour guide was excellent. He was informative and also took care of the people falling behind in the trip. We were joined by other guides, one at Marble Cave and the other at Gracanica Monestary. The guide at Marble cave was telling us about it in the Albanian langauge, which our tour guide was translating into English. The guides were very enthusiastic about the places they were telling us about. It was like an overdose of knowledge that day. We got a little late in our schedule and so we couldn’t really explore the Gracanica Monestary, we had to rush back to our destination after a few minutes.

Some photos from DayTrip taken by me:

<figure>
<img src="https://upload.wikimedia.org/wikipedia/commons/c/c2/Gra%C4%8Danica_Lake%2C_Kosovo.jpg" width="500">
<figcaption>Gracanica Lake</figcaption>
</figure>

<figure>
<img src="https://upload.wikimedia.org/wikipedia/commons/2/2e/Gra%C4%8Danica_Monastery_entry.jpg" width="500">
<figcaption>Gracanica Monestary</figcaption>
</figure>

<figure>
<img src="https://upload.wikimedia.org/wikipedia/commons/1/17/Views_from_the_Bear_Sanctuary_in_Pristina%2C_Kosovo.jpg" width="500">
<figcaption>Beautiful views seen from the Bear Sanctuary</figcaption>
</figure>

### Conference Dinner

The Conference Dinner was held outside of the DebConf venue in Alegria Hotel. Here the food was a disappointment for non-meat eaters/vegan as the restaurant only gave rice and some snacks. At the conference dinner, I liked the Flija. I still had some good company at the conference dinner so it was still fun.

### Keysigning

In Debian, people sign each other’s keys in order to make a [web of trust](https://en.wikipedia.org/wiki/Web_of_trust). It usually involves an ID check. People will usually ask you for your pasport to verify that it is really you. I saw many countries’ passport when keysigning, which was fun. After the ID check and verifying your key fingerprint, they sign your keys. Tobi helped me in configuring [caff](https://manpages.debian.org/unstable/signing-party/caff.1.en.html) so that I can easily sign keys, rather than doing it manually.

### Goodbye Kosovo

All in all, it was a great trip to Kosovo and a very unique one. I am already out of Kosovo as of writing this. I would like to visit Kosovo again in my life. It was so good. Thanks a lot to all the organizers, sponsors and people who made my time fun at the DebConf 22 in Kosovo.

Attending a DebConf is a learning opportunity on many fronts, be it the diversity among the people, cultures, food, opinion, or the software side of Debian. Writing about my experience has been a challenge in itself. Hopefully, I was able to pin down my experience as there was lot to share.

Looking forward to see you all in the [next edition of DebConf](https://wiki.debian.org/DebConf/23) which will take place in my country India.

Update on 06-August-2022: I got the reimbursement of all my travel expenses related to DebConf22 in just 4 days of filling the reimbursement form. So quick!
