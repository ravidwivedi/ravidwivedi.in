---
title: "Introducing Fediverse: A Decentralized Social Media"
date: 2022-10-29T14:44:56+05:30
draft: false
type: "post"
showTableOfContents: true
---
Finally, [Elon Musk is the owner of Twitter](https://www.nytimes.com/2022/10/27/technology/elon-musk-twitter-deal-complete.html). On the first day after the takeover, Twitter has already undergone [some major changes](https://www.nytimes.com/2022/10/28/business/twitter-elon-musk.html). If you are wondering what the big deal is, here is a reminder that [tech giants work for profit and control](https://jacobin.com/2018/02/elon-musk-hyperloop-public-transit-tech), rather than to make the world a better place. While I do not consider the previous CEO of Twitter to be good, the takeover by Musk may have made people more receptive to the ideas I am going to discuss.

## This is a problem of centralization

The takeover of Twitter exemplifies a classic issue with [centralized services](/glossary/#centralized-services). A centralized service may seem beneficial today, but it could be compromised in the future due to such takeovers. For instance, Facebook's acquisition of WhatsApp resulted in Facebook almost monopolizing messaging services. The dictator of such a service will shape policies according to their desires, potentially conflicting with user preferences. Moreover, when a large number of people rely on centralized services, a significant portion of global communications flows through one company, granting it [enormous control](https://medium.com/thrive-global/how-technology-hijacks-peoples-minds-from-a-magician-and-google-s-design-ethicist-56d62ef5edf3) over individuals. Users lack decision-making power over these services, and after years of building a profile, they may hesitate to delete their accounts due to the fear of losing followers and a means of contacting friends. Consequently, they become tied to the service and find it challenging to break free.

Facebook's outage [last year](/posts/what-does-facebook-outage-teach-us/) serves as a poignant reminder of the repercussions of depending on centralized services.

## Introducing Fediverse

<a title="Eukombos, CC0, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Fediverse_logo_proposal.svg"><img width="200" alt="Fediverse logo proposal" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/Fediverse_logo_proposal.svg/512px-Fediverse_logo_proposal.svg.png">
<figcaption>Fediverse logo</figcaption></a>

**Fediverse is NOT another company or startup; it is a community-driven project.**

Fediverse is a decentralized social network where users across different services can follow, boost posts, and communicate with each other, akin to how users with accounts on various email providers can email each other. Different services on Fediverse have distinct policies, allowing users to select a service that aligns with their needs. Anyone can self-host their instance of Fediverse or pay someone to host it for them, similar to any other task in the world. Fediverse is not owned by a single entity; instead, different instances have different administrators, typically managing the service in their spare time. Consider supporting them with a donation if you appreciate the service. Fediverse users are not bound to a specific service; when they switch providers, they can migrate their friends and followers, eliminating the need to start anew.

Fediverse comprises various service types. For instance, [Mastodon](https://joinmastodon.org/) resembles Twitter in terms of use case, [Friendica](https://friendi.ca/) is akin to Facebook, [Pixelfed](https://pixelfed.org/) focuses on image sharing like Instagram, and [PeerTube](https://joinpeertube.org/en) is a video-sharing platform, serving as an alternative to YouTube.

Each service on Fediverse is based on [free software](/free-software), enabling users to add features themselves. For instance, if Mastodon lacks a desired feature, users can modify and run their version of Mastodon on their server. They can also collaborate with others who share the same feature preference and collectively crowdfund to incorporate it.

![How the Fediverse connects](https://upload.wikimedia.org/wikipedia/commons/d/d0/How-the-Fediverse-connects.png)
*Source: [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:How-the-Fediverse-connects.png)*

Learn more about Fediverse [here](https://fediverse.info/), and refer to [this unofficial guide](https://fedi.tips/mastodon-and-the-fediverse-beginners-start-here/) to get started. We must advocate for our freedom. Consider taking a step by deleting your Twitter account or exploring Fediverse and encouraging your friends to join, reducing reliance on Twitter, Facebook, and Instagram. I recommend major organizations, which typically have resources, to transition to Fediverse, raising awareness among their users. Even if they are reluctant to delete their Twitter accounts, Fediverse can help reduce their dependence and, hopefully, attract more users.

Connect with me on Fediverse [here](https://toot.io/@ravi).

For further insights on this topic, read [this blog post](https://fsci.in/blog/self-hosting-and-federation/) by the Free Software Community of India.

