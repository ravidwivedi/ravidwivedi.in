---
title: "Fixing Mobile Data issue on Lineage OS"
date: 2024-03-01T14:34:08+05:30
draft: false
type: "post"
showTableOfContents: true
tags: ["tutorial", "lineage os", "fix"]
---
I have used LineageOS in many Android devices and face internet connectivity issues with mobile data. My mobile data never used to work properly in Xiaomi MI A2 on Lineage OS and my current phone OnePlus 9 Pro 5G. A few days ago, I met [contrapunctus](https://contrapunctus.codeberg.page) who has the same phone model with Lineage OS. He fixed this issue by comparing the settings of my phone with his.

In case, you are suffering from this issue, the following steps fixed the issue for me:

1. Navigate to Settings -> Network Settings -> Your SIM settings -> Access Point Names.
2. Click on the ‘+’ symbol to add a new access point.
3. In the Name section, you can input any name (e.g., `test`).
4. In the APN section, enter `www` and save the settings.

Check the screenshot below.

<figure>
  <img src="/images/APN-screenshot.png" width="250">
  <center><figcaption><h4>APN settings screenshot. Notice the circled entries.</h4></figcaption></center>
</figure>

Once you have added this new APN, ensure to select it from the list of available APNs. Following this configuration change, the issue got fixed. I hope this works for you :)
