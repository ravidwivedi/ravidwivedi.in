---
title: "How I deleted Google, Samsung apps from my Android without rooting"
date: 2021-07-27
---
In this post, I will write how I deleted (yes, deleted, uninstalled, gone! not disabled) Google apps, Samsung apps and Facebook app from my phone. These apps are pre-installed and they only give an option to "Diasble" these apps which does not do anything, it only stops showing them in the app drawer. These apps are dangerous to user's privacy and [record their whole lives](https://www.theguardian.com/commentisfree/2018/mar/28/all-the-data-facebook-google-has-on-you-privacy) through their phones. You do not need to know anything about the command line to follow this guide.
showTableOfContents: true

Sahil's blog post on [Mobile Made More Private](https://blog.sahilister.in/2021/05/mobile-made-more-private) was the one which guided me in this process. Be sure to check out that for more details and the [adb commands cheat sheet](https://christitus.com/debloat-android/) that Sahil mentions in that post.

<br> 

**A fair warning and comment from Chris Titus Tech:**

 It is dangerous to remove system apps that you don’t see in the app drawer. It can lead to you doing a full system recovery.

On Non-Rooted phones some applications are installed as root and error with “[DELETE_FAILED_INTERNAL_ERROR]”. Type this to bypass:

adb shell pm uninstall --user 0 <appname>

<br>

So, it is a "Do at your own risk" article. 

#### How I did it

I have a Samsung Galaxy S9+ phone with Google and Samsung's nonfree software installed in it. I did not yet root my phone to install a custom ROM. I have a Macbook which does not allow me to connect my Android using a USB cable, a step which is essential to run command line in your android to uninstall these apps. 

So, I connected my phone to a laptop in which Windows 10 was installed. The steps are similar and commands are same (except for the `grep` command which works in GNU/Linux but not in Windows, which I will mention) if you using GNU/Linux or macOS. 

These are the steps I followed in Windows 10: 

**Step 1** : Install chocolatey, a [freedom-respecting](https://github.com/chocolatey/choco/blob/master/LICENSE) command line program to download software in Widnows. The steps to install Chocolatey are [here](https://chocolatey.org/install). You don't need to visit that link, I will write all the steps for you.

Below are the steps for installing Chocolatey on Windows.

**Step 1a** : Press windows button on your keyboard and search for 'PowerShell'. Right click on PowerShell and click on an option which says run it in the administrator mode. 

**Step 1b** : In the PowerShell, run the command (which means, copy the following command and paste it in PowerShell and press enter on Keyboard):

`Get-ExecutionPolicy`

If it returns `Restricted`, run the command:

`Set-ExecutionPolicy AllSigned`

**Step 1c** : Run the following command: 

`Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))`

If you do not get any error message, congratulations! Chocolatey is installed in your system. 

Type `choco -?` in the command line and press enter. If there is no error, it means that Chocolatey has been installed in your system.

**Step 2** : We will install ADB Platform Tools. To install, run the command: 

`choco install adb`

**Step 3** : Setting up your Android phone

**3a** :  Open Settings on your phone, and select “About”.

**3b** : Tap on “Build number” seven times.

**3c** : Go back, and select “Developer options”.

**3d** : Scroll down, and check the “Android debugging” or “USB debugging” entry under “Debugging”.

**3e** : Plug your device into your computer.

**3f** : On the computer, open up a terminal/command prompt and type adb devices.

**3g** : A dialog should show on your device, asking you to allow usb debugging. Check “always allow”, and choose “OK”. 

**Step 4** : Run the command:

`adb devices`

This command should list the Android phone you connected to your computer. If it lists your phone, you are ready to proceed. Otherwise, your phone isn't connected and not ready to use adb commands.

**Step 5** : You can list all the packages of Google in your phone by using the command:

In Windows: `adb shell pm list packages | findstr google `

In GNU/Linux or macOS: `adb shell pm list packages | grep google ` 

 The following image has commands that can be used to uninstall Google apps.

 <figure>
  <img src="/images/adb-commands.jpg">
  <figcaption> Uninstall Google apps from Android using adb commands</figcaption>
</figure> 

<br>

#### Uninstalling the apps

Let's try and uninstall the YouTube app. Check the image above for the package name. The package name for YouTube is `com.google.android.youtube`. The following command should be the one to uninstall YouTube (same command for Windows, macOS, GNU/Linux): 

`adb shell pm uninstall -k --user 0 com.google.android.youtube`

If the command returns the message saying: `Success`, then you have successfully uninstalled YouTube.  

Similarly, you can uninstall any other app using this command by replacing `com.google.android.youtube` by the package name of the app.

For removing Google Play Store, I used the following command:

`adb shell pm uninstall -k --user 0 com.android.vending`

For me, it was no problem uninstalling Google Play Store but I have heard it can be risky. 

I have listed all the commands that I tried in my phone in [adb-commands-ravi.txt](/files/adb-commands-ravi.txt). Feel free to use some or all of these commands.

A trick to know the app ID for most of the apps is to visit their Google Play Store page. For example, visit the Facebook app on Google Play Store and look at the URL. Copy the part after `id=` which is `com.facebook.katana`. 


 <figure>
  <img src="/images/fb-on-play-store.png"  style="width:70%, height:70%">
  <figcaption> To retrieve package name of an app</figcaption>
</figure> 

Therefore, the command to uninstall Facebook will be:

`adb shell pm uninstall -k --user 0 com.facebook.katana` 

That's it for now.

Happy Hacking with (hopefully) less amount of tracking:)
