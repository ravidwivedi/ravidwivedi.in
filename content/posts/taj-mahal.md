---
title: "A visit to the Taj Mahal"
date: 2024-03-29T15:43:21+05:30
draft: false
type: "post"
tags: ["travel", "taj-mahal", "agra", "india"]
showTableOfContents: true
---
## Introduction

I visited the Taj Mahal this month with my friend Badri. Taj Mahal is a major tourist attraction, getting 7-8 million visitors per year. It is one of the most beautiful pieces of architecture.

## Journey to Agra

Taj Mahal is in the city of Agra, which is 188 km from Delhi by train. We had booked a train by the name of Taj Express from Hazrat Nizamuddin station in Delhi to Agra Cantt railway station. The night before the journey, we stayed in a retiring room at Old Delhi railway station because we could not get one at Hazrat Nizamuddin station. Retiring rooms are accommodations for the passengers at the railway stations, which require a confirm train ticket to stay. 

Of all the retiring rooms I have been to, locating the ones at Old Delhi station was the most challenging. I recommend reading Badri's [blog post](https://badrihippo.thekambattu.rocks/blog/retiring-rooms.html), where he details our experience with this and other retiring rooms.

{{< figure loading="lazy" src="/images/taj-mahal/Retiring-room-in-Delhi.JPG" title="Our retiring room at the Old Delhi Railway Station." width="300">}}

The next day, we barely reached Hazrat Nizamuddin station in time for the train. However, the train had not yet arrived at the station and was delayed by half an hour.

## Arrival in Agra

We reached Agra Cantt station at 10:30 hours, where we stayed in a retiring room. Before heading out to Taj Mahal, we rested in our rooms for a couple of hours, followed by having lunch at the station. As we came out of the station, we were approached by an autorickshaw driver who quoted the price to Taj Mahal as 150 INR. I negotiated the price down to 60 INR, which the driver agreed to on the condition of sharing the ride with other passengers. When we realized he was not making any effort to bring in more passengers to share the ride with, we walked away.

As we came out of the station complex, an autorickshaw driver offered a ride to the Taj Mahal for 20 INR per person on a sharing basis, or 100 INR for a reserved ride. We agreed to share the ride with other passengers for 20 INR, but the driver started driving as soon as we got in. We mistook the other person in the autorickshaw for a passenger we were sharing the ride with, but later found out that they were with the driver. Upon reaching the outer gate of the Taj Mahal, we paid him 40 INR as agreed, but he insisted that we had reserved the auto. We told him that we did not reserve the auto, but rather chose the sharing option. He insisted on this for some time. I suspect it was a scam, but we just walked away, and he didn't pursue us further.

## Exploring the Taj Mahal

The autorickshaw dropped us at one of the outer gates, from where we had to walk about 500 meters to reach the ticket counter just outside the west gate. We bought tickets worth 250 INR per person, which also allowed us to enter the mausoleum. Then we proceeded to the security check before entering the Taj Mahal complex.

{{< figure loading="lazy" src="/images/taj-mahal/security-outside-taj-mahal.jpg" title="Security outside the Taj Mahal complex." width="300" >}}

{{< figure loading="lazy" src="/images/taj-mahal/entry-to-taj-mahal.JPG" title="This red-colored building is the entrance to where you can see the Taj Mahal." width="300">}}

{{< figure loading="lazy" src="/images/taj-mahal/taj-mahal.JPG" title="Taj Mahal." width="300" >}}

Upon entering, we saw red sandstone walls on three sides enclosing the Taj Mahal complex. We took photos using my phone and a Fujifilm camera. Later, we learned that we needed to cover our shoes before entering the mausoleum. We also saw a few people barefoot, but we couldn't find a place to leave our shoes. We came out of the whole complex at 18:00 hours and had snacks with tea at a nearby shop. I also bought a fridge magnet as a souvenier for 30 INR.

{{< figure loading="lazy" src="/images/taj-mahal/shoe-covers.jpg" title="Shoe covers for going inside the mausoleum." width="300" >}}

{{< figure loading="lazy" src="/images/taj-mahal/taj-mahal-from-another-angle.jpg" title="Taj Mahal from a side angle." width="300" >}}

Our next destination was Jaipur and we had booked our seats in a train from Agra Cantt station. We decided to walk towards the station, hoping to have dinner along the way. However, we could not find a place to eat. Since we enjoyed the lunch at the station earlier, we went there for dinner as well, after which we boarded our train to Jaipur. On our way back to the station, I found the [bus station](https://www.openstreetmap.org/way/1260758137) for the Taj Mahal.

## Expenses

These were our expenses per person:

| **Description**                       		| **Amount (Indian Rupees)**         	|
|-----------------------------------------------	|------------------------------------	|
| Retiring room at Delhi Railway Station (12 hours) 	| <p align="right">131</p>           	|
| Train ticket from Delhi to Agra (Taj Express)       	| <p align="right">110</p>        	|
| Retiring room at Agra Cantt station for 12 hours:     | <p align="right">450</p>         	|
| Auto-rickshaw to Taj Mahal      			| <p align="right">20</p>           	|
| Taj Mahal ticket (including entry to the mausoleum)	| <p align="right">250</p>           	|
| Food     						| <p align="right">350</p>           |
| **Total**                                     	| <p align="right"><b>1,311</b></p> |
