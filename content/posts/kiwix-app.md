---
title: "How To Read Wikipedia Offline"
date: 2022-03-31T12:56:27+05:30
draft: false
type: "post"
showTableOfContents: true
---
Recently I came to know about [freedom-respecting](/free-software) [Kiwix app](https://kiwix.org) and I found it cool. It is a reader for offline reading knowledge sources like [Wikipedia](https://wikipedia.org) or [Project Gutenberg](https://www.gutenberg.org/). There are many projects which you can download for offline reading, like AskUbuntu, sections of Wikipedia-- like, Chemistry, History, Sociology, etc. RationalWiki (my favorite), Ted Talks, Khan Academy, ArchWiki, etc. You can download Wikivoyage and have travel guides for offline reading, as you might not have good internet access on the go. The files are downloaded in the [free format](/glossary/#free-format) [.zim](https://en.wikipedia.org/wiki/ZIM_(file_format)) which are readable by the Kiwix reader. The files are compressed, which saves disk space and internet data while downloading. Further, the zim files make it easy to index, search, and selectively decompress. It can also export files into HTML and PDF formats.

<figure> 
<img src="/images/kiwix-screenshot.png" style="width:256px">
<figcaption><i>Screenshot of Kiwix Android app. </i> License: <a href="https://creativecommons.org/licenses/by-sa/4.0/">CC-BY-SA-4.0</a> </figcaption>
</figure>

The app is [available for download](https://www.kiwix.org/en/download/) for all the major operating systems-- Android, GNU/Linux, iOS, Windows, macOS, Raspberry Pi. In the Android app, you can download content by going to the download section and clicking on the website you would like. For Desktop, just download a zim file from [the Kiwix library](https://library.kiwix.org/) and open it in the Kiwix app. The project also has a nice [wiki documentation](https://wiki.kiwix.org).

<figure> 
<img src="/images/kiwix-desktop.png" style="width:700px">
<figcaption><i>Screenshot of Kiwix app in PureOS, with offline Wikipedia.</i> License: <a href="https://creativecommons.org/licenses/by-sa/4.0/">CC-BY-SA-4.0</a> </figcaption>
</figure>

The features make the app very usable for viewing the content offline. Getting the Wikipedia knowledge at one place, while clicking the links referring to the respective pages inside the same offline directory makes it very convenient to use. Check the [features page](https://wiki.kiwix.org/wiki/Features) for more details.

As much as I like offline reading due to internet being a distraction, other use cases are even better. The zim files can be downloaded and taken to places which are cut off from internet access, and spread knowledge there. Defector NGOs [use Kiwix-desktop to smuggle](https://www.kiwix.org/en/about/stories/north-korea/) knowledge of the outside world to North Korea, where [internet is almost non-existent](https://en.wikipedia.org/wiki/Internet_in_North_Korea) with [extreme amounts of censorship](https://en.wikipedia.org/wiki/Censorship_in_North_Korea). In Ghana, where schools have access to computers but not internet, [a project](https://www.kiwix.org/en/about/stories/ghana/) downloaded Kiwix and the full English Wikipedia and Wiktionary on a portable hard drive and transferred it to school computer labs. [Quoting the project](https://www.kiwix.org/en/about/stories/ghana/), "Fewer than 10 percent of students had ever accessed Wikipedia before the training took place. Now half the students say the new resource has influenced their studies." 

You can [use a Raspberry Pi](https://www.kiwix.org/en/cardshop-access/) to create a local server which can be used to access the downloaded content from 25 devices, which is another cool feature.

When I downloaded the 87 GB Wikipedia zim file, I ran into an immediate problem: I couldn't copy it into my USB drive due to FAT32 filesystem which can’t store files bigger than 4 GB. So, I backed up all the data on my USB drive and [used this tutorial](https://newtocode.wordpress.com/2014/02/12/format-usb-as-exfat-in-ubuntu-13-10-terminal/) to format my USB drive into exFAT format, which supports larger file sizes.

A drawback in the current form is that incrememtal updates are not available, which means if you download Wikipedia on 1 Feb 2022, and now you want the changes till date, then you cannot just update the changes, you need to download from scratch. The project is working on such features.

I already started using Kiwix app almost exclusively for offline reading. Try it and I am sure you will find something useful for you. 

**The project relies on user donations, so I would like to urge you to support the Kiwix project by [making donations](https://www.kiwix.org/en/support/).**

Finally, I leave you with some more screenshots of Kiwix. Enjoy :)

<figure> 
<img src="/images/kiwix-askubuntu.png" style="width:500px">
<figcaption><i>Me reading AskUbuntu forum offline in my machine.</i> License: <a href="https://creativecommons.org/licenses/by-sa/4.0/">CC-BY-SA-4.0</a> </figcaption>
</figure>

<figure> 
<img src="/images/kiwix-home.png" style="width:500px">
<figcaption><i>Kiwix Desktop homepage.</i> License: <a href="https://creativecommons.org/licenses/by-sa/4.0/">CC-BY-SA-4.0</a> </figcaption>
</figure>

<figure> 
<img src="/images/kiwix-rationalwiki.png" style="width:500px">
<figcaption><i>Reading RationalWiki on Kiwix Android.</i> License: <a href="https://creativecommons.org/licenses/by-sa/4.0/">CC-BY-SA-4.0</a> </figcaption>
</figure>

<figure> 
<img src="/images/kiwix-wikimed.png" style="width:500px">
<figcaption><i>Wikimed on Kiwix app.</i> License: <a href="https://creativecommons.org/licenses/by-sa/4.0/">CC-BY-SA-4.0</a> </figcaption>
</figure>
