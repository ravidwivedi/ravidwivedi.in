---
title: "Backstory Behind Prav App Project"
date: 2023-01-18T21:02:40+05:30
draft: false
tags: ["prav", "freesoftware", "XMPP"]
showTableOfContents: false
type: "post"
---
In 2020, I uninstalled WhatsApp due to its proprietary nature and being backed by a surveillance company. In my quest to find alternatives, I came across many messengers, before finally settling on Matrix and XMPP, both of which are decentralized chat protocols built on freedom-respecting software.

But chat applications are only as good as the number of people using them, and it was not easy to onboard people to XMPP or Matrix.

Creating an account on either of them requires installing an app, followed by choosing a server, a username, and a password. This onboarding process is relatively difficult compared to more commonly-known messengers like WhatsApp and Telegram. It was suitable for privacy-conscious users, but not for the majority of people who are not so privacy-conscious, or who just found it too much effort to invest into a new app.

At some point, Praveen (of FSCI fame) introduced me to Quicksy - an XMPP client which provides two features found in WhatsApp -
1. easier registration, allowing users to register by entering a phone number and an OTP;
2. easier contact discovery, which automatically adds people to your Quicksy contact list if you have their phone number in your phone book.

These made onboarding easier, and I was successfully able to use Quicksy to onboard users.

At the time, we were also running our own XMPP services at poddery.com and disap.in. These services were run by volunteers, and there had been instances where it took us months to fix something.

Further, we found that the XMPP clients weren’t consistent in their features across platforms. For example, Kaidan didn’t have OMEMO encryption, while Gajim doesn’t support calls. So, if a user wants to use a desktop client to make XMPP calls, they need to figure out that they have to install Dino (which is only available on GNU/Linux). Having a consistent brand across all platforms helps newbies, as it might be hard for them to wrap their head around using Dino to talk to Conversations.

One day, when Praveen’s friend installed Quicksy to share pictures with him, he gave him an idea to make a business out of it. Praveen also thought it would be better to market our own XMPP service rather than Quicksy, giving us more control over the privacy policy and service operations. This is when he asked me if I would like to get involved, to which I immediately agreed.

Praveen had many years of experience running XMPP services, albeit in a volunteer setting. He thought of experimenting with paid sysadmins rather than volunteers. This is how the Prav project was born. The phone number and contact discovery part was inspired by the Quicksy app, and the idea of consistent branding across platforms was inspired by Snikket. We also added the idea of making it subscription based, which can help us fund sysadmins and allocate funds towards marketing.

A few months later, we decided to make it a cooperative so that the decision-making is democratic (one person, one vote, as opposed to companies in which each person has voting power proportional to the number of shares they hold). Nagarjuna proposed we run a beta service to attract people to pledge the cooperative membership. This gave birth to our beta app and service.

Free software gives users the freedom to adapt the software - but in practice, only programmers or resourceful entities like governments or corporations get to exercise this freedom. With the cooperative model of the Prav project, we want to empower members to collectively decide upon which features to implement, followed by raising funds for developers to implement them.

I am excited to see how this experiment will turn out.
