---
title: "The Arduous Luxembourg Visa Process"
date: 2025-01-21T16:15:57+05:30
draft: false
type: "post"
tags: ["schengen-visa", "luxembourg", "visa", "vfs-global"]
---
In 2024, I was sponsored by [The Document Foundation (TDF)](https://www.documentfoundation.org/) to attend the [LibreOffice annual conference](https://conference.libreoffice.org/2024/) in Luxembourg from the 10th to the 12th of October. Being an Indian passport holder, I needed a visa to visit Luxembourg. However, due to my Kenya trip coming up in September, I ran into a dilemma: whether to apply before or after the Kenya trip. 

To obtain a visa, I needed to submit my application with VFS Global (and not with the Luxembourg embassy directly). Therefore, I checked the VFS website for information on [processing time](https://www.vfsglobal.com/one-pager/luxembourg/india/english/index.html), which says:

> As a rule, the processing time of an admissible Schengen visa application should not exceed 15 calendar days (from the date the application is received at the Embassy).

It also mentions:

> If the application is received less than 15 calendar days before the intended travel date, the Embassy can deem your application inadmissible. If so, your visa application will not be processed by the Embassy and the application will be sent back to VFS along with the passport.

If I applied for the Luxembourg visa before my trip, I would run the risk of not getting my passport back in time, and therefore missing my Kenya flight. On the other hand, if I waited until after returning from Kenya, I would run afoul of the aforementioned 15 working days needed by the embassy to process my application. 

I had previously [applied for a Schengen visa for Austria](/posts/austrian-visa-refusal-jan-2024), which was completed in 7 working days. My friends who had been to France told me they got their visa decision within a week. So, I compared Luxembourg's application numbers with those of other Schengen countries. In 2023, Luxembourg [received 3,090](https://www.schengenvisainfo.com/statistics/luxembourg/) applications from India, while Austria [received 39,558](https://www.schengenvisainfo.com/statistics/austria/), Italy [received 52,332](https://www.schengenvisainfo.com/statistics/italy/) and France [received 176,237](https://www.schengenvisainfo.com/statistics/france/). Since Luxembourg receives a far fewer number of applications, I expected the process to be quick.

Therefore, I submitted my visa application with VFS Global in Delhi on the 5th of August, giving the embassy a month with 18 working days before my Kenya trip. However, I didn't mention my Kenya trip in the Luxembourg visa application.

For reference, here is a list of documents I submitted:

- Passport
- Photocopy of passport data pages
- Visa application form
- One photograph
- Visa appointment confirmation 
- Cover letter
- Return flight reservations
- Hotel bookings
- Invitation letter from the conference organizer, TDF
- Confirmation from The Luxembourg Convention Bureau G.I.E - the venue
- Last three months bank account statement with bank seal
- Travel insurance
- Income Tax Return Statement
- Monthly payslips for the last three months

I submitted 'flight reservations' instead of 'flight tickets'. It is because, in case of visa rejection, I would have lost a significant amount of money if I booked confirmed flight tickets. The embassy also recommends the same. After the submission of documents, my fingerprints were taken.

The expenses for the visa application were as follows:

| Service Description   	| Amount (INR)  |
| :---    		   	| ---:     	|
| Visa Fee 			| 8,114 	|
| VFS Global Fee 		| 1,763 	|
| Courier 			|  800	 	|
| **Total**			| **10,677** 	|

Going by the emails sent by VFS, my application reached the Luxembourg embassy the next day. Fast-forward to the 27th of August — 14th day of my visa application. I had already booked my flight ticket to Nairobi for the 4th of September, but my passport was still with the Luxembourg embassy, and I hadn’t heard back. In addition, I also obtained Kenya's eTA and got vaccinated for Yellow Fever, a requirement to travel to Kenya.

In order to check on my application status, I gave the embassy a phone call, but missed their calling window, which was easy to miss since it was only 1 hour - 12:00 to 1:00 PM. So, I dropped them an email explaining my situation. At this point, I was already wondering whether to cancel the Kenya trip or the Luxembourg one, if I had to choose.

After not getting a response to my email, I called them again the next day. The embassy told me they would look into it and asked me to send my flight tickets over email. One week to go before my flight now.

I followed up with the embassy on the 30th by a phone call, and the person who picked up the call told me that my request had already been forwarded to the concerned department and is under process. They asked me to follow up on Monday, 2nd September.

During the visa process, I was in touch with three other Indian attendees.[^1] In the meantime, I got to know that all of them had applied for a Luxembourg visa by the end of the month of August.

Back to our story, over the next two days, the embassy closed for the weekend. I began weighing my options. On one hand, I could cancel the Kenya trip and hope that Luxembourg goes through. Even then, Luxembourg wasn't guaranteed as the visa could get rejected, so I might have ended up missing both the trips. On the other hand, I could cancel the Luxembourg visa application and at least be sure of going to Kenya. However, I thought it would make Luxembourg very unlikely because it didn't leave 15 working days for the embassy to process my visa after returning from Kenya. I also badly wanted to attend the LibreOffice conference because I [couldn't make it](/posts/couldnt-attend-libreoffice-conference/) two years ago. Therefore, I chose not to cancel my Luxembourg visa application. I checked with my travel agent and learned that I could cancel my Nairobi flight before September 4th for a cancelation fee of approximately 7,000 INR.

On the 2nd of September, I was a bit frustrated because I hadn't heard anything from the embassy regarding my request. Therefore, I called the embassy again. They assured me that they would arrange a call for me from the concerned department that day, which I did receive later that evening. During the call, they offered to return my passport via VFS the next day and asked me to resubmit it after returning from Kenya. I immediately accepted the offer and was overjoyed, as it would enable me to take my flight to Nairobi without canceling my Luxembourg visa application. However, I didn't have the offer in writing, so it wasn't clear to me how I would collect my passport from VFS. The next day, I would receive it when I would be on my way to VFS in the form of an email from the embassy which read:

> Dear Mr. Dwivedi,
>
> We acknowledge the receipt of your email.
> 
> As you requested, we are returning your passport exceptionally through VFS, you can collect it directly from VFS Delhi Center between 14:00-17:00 hrs, 03 Sep 2024. Kindly bring the printout of this email along with your VFS deposit receipt and Original ID proof.
>
> Once you are back from your trip, you can redeposit the passport with VFS Luxembourg for our processing.
>
> With best regards,<br>
> Consular Section
>
> GRAND DUCHY OF LUXEMBOURG<br>
> Embassy in New Delhi

I took a printout of the email and submitted it to VFS to get my passport. This seemed like a miracle - just when I lost all hope of making it to my Kenya flight and was mentally preparing myself to miss it, I got my passport back "exceptionally" and now I had to mentally prepare again for Kenya. I had never heard of an embassy returning passport before completing the visa process before. The next day, I took my flight to Nairobi as planned. In case you are interested, I have written two blog posts on my Kenya trip - one on the [OpenStreetMap conference in Nairobi](/posts/sotm-2024/) and the other on [my travel experience in Kenya](/posts/kenya-trip).

After returning from Kenya, I resubmitted my passport on the 17th of September. Fast-forward to the 25th of September; I didn't hear anything from the embassy about my application process. So, I checked with TDF to see whether the embassy reached out to them. They told me they confirmed my participation and my hotel booking to the visa authorities on the 19th of September (6 days ago). I was wondering what was taking so long after the verification.

On the 1st of October, I received a phone call from the Luxembourg embassy, which turned out to be a surprise interview. They asked me about my work, my income, how I came to know about the conference, whether I had been to Europe before, etc. The call lasted around 10 minutes. At this point, my travel date - 8th of October - was just two working days away as the 2nd of October was off due to Gandhi Jayanti and 5th and 6th October were weekends, leaving only the 3rd and the 4th. I am not sure why the embassy saved this for the last moment, even though I submitted my application 2 months ago. I also got to know that one of the other Indian attendees missed the call due to being in their college lab, where he was not allowed to take phone calls. Therefore, I recommend that the embassy agree on a time slot for the interview call beforehand.

Visa decisions for all the above-mentioned Indian attendees were sent by the embassy on the 4th of October, and I received mine on the 5th. For my travel date of 8th October, this was literally the last moment the embassy could send my visa. The parcel contained my passport and a letter. The visa was attached to a page in the passport. I was happy that my visa had been approved. However, the timing made my task challenging. The enclosed letter stated:

> **Subject: Your Visa Application for Luxembourg**

> Dear Applicant,
>
> We would like to inform you that a Schengen visa has been granted for the 8-day duration from 08/10/2024 to 30/10/2024 for <u>conference purposes in **Luxembourg**</u>.
>
> **You are requested to report back** to the Embassy of Luxembourg in New Delhi through an email (email address redacted) after your return with the following documents:
> - Immigration Stamps (Entry and Exit of Schengen Area)
> - Restaurant Bills
> - Shopping/Hotel/Accommodation bills
>
> <u>Failure to report</u> to the Embassy after your return will be taken into consideration for any further visa applications.

I understand the embassy wanting to ensure my entry and exit from the Schengen area during the visa validity period, but found the demand for sending shopping bills excessive. Further, not everyone was as lucky as I was as it took a couple of days for one of the Indian attendees to receive their visa, delaying their plan. Another attendee had to send their father to the VFS center to collect their visa in time, rather than wait for the courier to arrive at their home.

Foreign travel is complicated, especially for the citizens of countries whose passports and currencies are weak. Embassies issuing visas a day before the travel date doesn’t help. For starters, a last-minute visa does not give enough time for obtaining a forex card as banks ask for the visa. Further, getting foreign currency (Euros in our case) in cash with a good exchange rate becomes difficult. As an example, for the Kenya trip, I had to get US Dollars at the airport due to the plan being finalized at the last moment, worsening the exchange rate. Back to the current case, the flight prices went up significantly compared to September, almost doubling. The choice of airlines also got narrowed, as most of the flights got booked by the time I received my visa. With all that said, I think it was still better than an arbitrary rejection.

[^1]: Thanks to Sophie, our point of contact for the conference, for putting me in touch with them.

**Credits: [Contrapunctus](https://contrapunctus.codeberg.page/), [Badri](https://badrihippo.thekambattu.rocks/), Fletcher, Benson, and [Anirudh](https://codeberg.org/ravidwivedi/ravidwivedi.in/issues/5) for helping with the draft of this post.**
