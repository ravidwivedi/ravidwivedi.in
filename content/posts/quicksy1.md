---
title: "Use Quicksy"
date: 2022-01-04
draft: false
type: "post"
showTableOfContents: true
---
Just use Quicksy app. It is available on [Google Play Store](https://play.google.com/store/apps/details?id=im.quicksy.client&hl=en_IN&gl=US). Download it and share it with your friends and spread happiness.  
