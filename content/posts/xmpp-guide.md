---
title: "A guide to set up your XMPP account"
date: 2021-07-24
draft: false
type: "post"
showTableOfContents: true
---
This is a guide to set up an account on [XMPP](https://www.xmpp.org/), which is a [federated](/glossary/#federated-services) chatting system.

You can watch this video tutorial to create an XMPP account without giving your phone number to the service:

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://videos.fsci.in/videos/embed/e6c2158a-7d6e-4108-942c-ef616530301e" frameborder="0" allowfullscreen></iframe>

I will use the [freedom-respecting](/free-software) software [Conversations app](https://conversations.im) for demonstration. Conversations app is available for Android only. Conversations is a paid app on Google Play Store. If you would like to support the development of Conversations app, then you can download it from Google Play Store. Otherwise, you can follow the steps below to download Conversations app free of cost. If you would like to download a free-of-cost XMPP app from Play Store, then check out [Blabber](https://www.blabber.im/EN.html) app.

Currently, Blabber app is very good for XMPP. It has some more functionality than Conversations app and these features are usually requested by users in the Conversations app. 

Some examples of XMPP apps for other platforms are- [Dino](https://dino.im) for GNU/Linux, [Gajim](https://gajim.org) for Windows and GNU/Linux, [Monal](https://monal.im) for macOS and iOS.  You can choose any XMPP app, and this guide probably works for all of them. A list of XMPP apps is [here](https://xmpp.org/software/clients.html) and all of them support OMEMO encryption by default.

You don't need to give any personal details, like phone number or email, to create an account. If you would like to register an XMPP account using your phone number (similar to how you set up in WhatsApp), then you can try [Quicksy app](https://quicksy.im), which respects your freedom as you can still connect to people using any XMPP app and XMPP service provider (remember that XMPP is federated!).

Steps to set up an account on Conversations app: 

Step 1: Download F-Droid from [here](https://f-droid.org/).

<img src="/images/f-droid-download.jpeg" width="250" height="350">

 F-Droid is a repository of [free software](https://www.gnu.org/philosophy/free-software-even-more-important.html) apps. It is a [privacy respecting](https://www.f-droid.org/en/2019/05/05/trust-privacy-and-free-software.html) alternative of Google Play Store. One difference is that F-Droid only contains apps which are freedom-respecting (they respect all four freedoms mentioned [here](https://gnu.org/philosophy/free-sw.html)) and verified by the F-Droid community for malicious features. 

F-Droid will also ensure that your app receives all the future updates. F-Droid community [also builds apps from the source code](https://www.f-droid.org/en/2019/05/05/trust-privacy-and-free-software.html) which ensures that the source code indeed matches the app you download in your system. 

Step 2: Search for "Conversations" app in F-Droid and install it.

<img src="/images/download-conversations.jpeg" width="250" height="350">

Step 3: Register an XMPP account on any xmpp server. You can choose an XMPP service provider from [this URL](https://compliance.conversations.im/). It will give you a list of 5 servers that you can register on, and then will give you 5 different random servers upon refreshing the page.

Step 4: Open the Conversations app and then click on 'I already have an account' option.

<img src="/images/conversations-1.jpeg" width="250" height="350">

Step 5: In the XMPP address option, put your xmpp address, which will be of the format username@domain  -- depending on which site you used to sign up. Click 'Next'.

<img src="/images/conversations-2.jpeg" width="250" height="350">

Step 6: You can add a profile picture if you wish and then click 'Publish'. If you do not want to add a profile picture, select 'Skip'.

<img src="/images/conversations-3.jpeg" width="250" height="350">

Step 7: Conversations app will ask you permission to access your contacts. You can click on 'Deny'. And it will then ask to stop battery optimization for the app. You can enable Conversations to run in the background. 

<img src="/images/conversations-4.jpeg" width="250" height="350">

Step 8:  Conversations app will then ask you to stop battery optimization for the app. I suggest you to allow Conversations to run without optimizing the battery. The developer of Conversations app has explained how this permission has [virtually zero impact on battery life](https://gultsch.de/xmpp_2016.html). 

<img src="/images/conversations-5.jpeg" width="250" height="350">

Step 9: To add a contact, press the '+' button at the bottom of the Conversations app and click on 'Add Contact'.

<img src="/images/conversations-6.jpeg" width="250" height="350">


Step 10: Type the XMPP address of the XMPP user you want to contact. In the image, I have entered my own XMPP address - ravi@poddery.com - as an example. 

<img src="/images/conversations-7.jpeg/" width="250" height="350">

Step 11: Just send an introduction message to your friend to exchange your OMEMO keys. 

<img src="/images/conversations-8.jpeg" width="250" height="350">

Congratulations on setting up your XMPP account. All the messages will be OMEMO encrypted by default -- means that only you and the person you are exchanging messages with can read them. Conversations app also support voice call and video call to any XMPP app that supports the same. 

  <img src="/images/shredder.png" width="250" height="350">
