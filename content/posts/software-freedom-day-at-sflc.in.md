---
title: "Software Freedom Day at sflc.in"
date: 2023-10-23T03:25:34+05:30
draft: false
tags: ["sflc.in", "freesoftware", "events"]
showTableOfContents: true
type: "post"
---
Software Freedom Law Center, India, also known as [sflc.in](https://sflc.in), organized an event to celebrate the [Software Freedom Day](https://sflc.in/sflc-in-celebrates-software-freedom-day2023/) on 30th September 2023. I, [Sahil](https://sahilister.in), [Contrapunctus](https://contrapunctus.codeberg.page/) and Suresh joined. The venue was at the SFLC India office in Delhi. The sflc.in office was on the second floor of what looked like someone's apartment:). I also met Chirag, Orendra, Surbhi and others.

My plan was to have a stall on [LibreOffice](https://libreoffice.org) and [Prav app](https://prav.app) to raise awareness about these projects. I didn't have QR code for downloading prav app printed already, so I asked the people at sflc.in if they can get it printed for me. They were very kind and helped me in getting a color printout for me. So, I got a stall in their main room. Surbhi was having an Inkscape stall next to mine and gave me company.  People came and asked about the prav project and then I realized I was still too tired to explain the idea behind the prav project and about LibreOffice (after a long Kerala trip). We got a few prav app installs during the event, which is cool.

{{< figure src="/images/sflcin-sfd/stall.jpg" title="My stall. Photo credits: Tejaswini." loading="lazy" >}}

Sahil had a Debian stall, and contrapunctus had an OpenStreetMap stall. After about an hour, [Revolution OS](https://en.wikipedia.org/wiki/Revolution_OS) was screened for all of us to watch, along with popcorn. The documentary gave an overview of history of Free Software Movement. The office had a kitchen where fresh chai was being made and served to us. The organizers ordered a lot of good snacks for us.

{{< figure src="/images/sflcin-sfd/front-desk.jpg" title="Snacks and tea at the front desk. CC-BY-SA 4.0 by Ravi Dwivedi." loading="lazy" >}}

I came out of the movie hall to take more tea and snacks from the front desk. I saw a beautiful painting was hanging at the wall opposite to the front desk and Tejaswini (from sflc.in) revealed that she had made it. The tea was really good as it was freshly made in the kitchen.

After the movie, we played a game of pictionary. We were divided into two teams. The game goes as follows: A person from a team is selected and given a term related to freedom respecting software written on a piece of paper, but concealed from other participants. Then that person draws something on the board (no logo, no alphabets) without speaking. If the person's team correctly guesses the term, the team gets one step ahead on the leaderboard. The team that reaches the finish line wins.

I recall some fun Pictionary rounds. For example, the one in the picture below seemed far from the word "Wireguard," but someone from the team still guessed that word. Our team won in the end \o/.

{{< figure src="/images/sflcin-sfd/fun-pictionary.jpg" title="Pictionary drawing nowhere close to the intended word Wireguard :), which was guessed. Photo by Ravi Dwivedi, CC-BY-SA 4.0." loading="lazy" >}}

Then, we posed for a group picture. At the end, SFLC.in had a delicious cake in store for us. They also had some merchandise available, such as handbags, T-shirts, etc., which we could take if we made a donation to SFLC.in. I "bought" a handbag with "Ban Plastic, not Internet" written on it in exchange for a donation. I hope that gives people around me a powerful message :)

{{< figure src="/images/sflcin-sfd/group-photo.jpg" title="Group photo. Photo credits: Tejaswini." loading="lazy" >}}

{{< figure src="/images/sflcin-sfd/cake.jpg" title="Tasty cake. CC-BY-SA 4.0 by Ravi Dwivedi." loading="lazy" >}}

{{< figure src="/images/sflcin-sfd/merchandise.jpg" title="Merchandise by sflc.in. CC-BY-SA 4.0 by Ravi Dwivedi." loading="lazy" >}}

Overall, sflc.in hosted a fantastic event!
