---
title: "What is DRM and how it threatens your freedom"
draft: false
type: "post" 
date: 2021-09-23
showTableOfContents: true
---

Ever used Spotify, Apple Music, Netflix or Hotstar? Ever wondered why you cannot copy the files from them and share it with someone? Ever wondered why you cannot download the files in your device and transfer in another device or play them in any other media player? Let's be clear on this: Downloading means that the file is now in your device and you can play/view it in any app of your choice. Download does not mean that you can only view the file in that particular app(like Spotify or Netflix) for offline use. 

The answer is DRM.  

DRM is usually known as Digital Rights Management by publishers, record labels and streaming dis-services. The long form [Digital Restrictions Management is closer to reality](https://www.gnu.org/philosophy/words-to-avoid.en.html#DigitalRightsManagement). 

DRM basically means digital files(any form of artwork, PDF, music files or software) are encrypted in such a way that users cannot copy them. This threatens user's freedom because technology allows us to copy files and share with others. What is the point of "technological progress"(being able to copy files is an example of technological progress) if you cannot even use it?  

Copying is an inherent nature of digital files to the extent that it reminds of the [quote by Bruce Schneier](https://www.schneier.com/essays/archives/2006/09/quickest_patch_ever.html), "Trying to make digital files uncopyable is like trying to make water not wet." Also, since we can copy files without any marginal cost and share it to everyone, it raises a fundamental moral question, quoting [Eben Moglen](http://old.law.columbia.edu/publications/maine-speech.html), "If I can provide to everyone all goods of intellectual value or beauty, for the same price that I can provide the first copy of those works to anyone, why is it ever moral to exclude anyone from anything? If you could feed everyone on earth at the cost of baking one loaf and pressing a button, what would be the moral case for charging more for bread than some people could afford to pay?"

The usual logic in favor of DRM is that artists lose money when someone copies a file and share it with someone. This argument assumes that everyone who has a copy of the file would have paid for a copy if sharing was prohibited. In reality, a lot of files would not even have been sold. Imagine a friend shared a book with you and you read it. Would you have read all such books shared with you if sharing was prohibited? When someone refuses sharing a book or a music file with you, do you go to market and purchase that thing? Also, when the marginal cost of copying is zero, nobody is losing money when you copy the file. Compare this situation with physical objects like chairs, noodles, clothes etc. Every piece of that physical object has a production cost while for software and digital files,the cost is only in the development of that file or software but every copy is effectively free-of-cost(given that computers and mobile phones are household items that people use for personal use anyway, so the cost of device is not included in the copying).  

Note that in the current system, only well-established artists really make money. Usually, the [artists do not get fair compensation](https://www.cnet.com/tech/home-entertainment/is-spotify-unfair-to-musicians/) and the streaming companies are the one earning from artists' work. These DRM imposing dis-services act like a middle agency between users and artists, effectively robbing user's freedom and artists' pay.  

Should we prohibit users from copying files and sharing them OR should we change our business model and allow them to share the files? Since, DRM benefits only publishers, streaming services, record labels and the like, while others receives discrimation, exclusion and lack of freedom to use technology, I think the the business model needs to change rather than restrciting users from sharing. 

DRM is also being used in educational resources now-a-days which excludes people who can't afford them or people who value freedom and won't use files containing DRM. 

DRM is already being used to [discriminate against the poor](https://libredd.it/r/ABoringDystopia/comments/otxab1/cyborg_cups_to_save_000001_worth_of_free_refills/) in many ways.

When a DRM imposing service gets shut down, users lose access to all their data they purchased from that service. For example,Yahoo music used to distribute music files with DRM and [when it shut down](https://arstechnica.com/uncategorized/2008/07/drm-still-sucks-yahoo-music-going-dark-taking-keys-with-it/), users could not backup their files and lost access to all the music they purchased. Another example is when Microsoft's ebook store closed, [all the users lost their ebooks](https://www.bbc.com/news/technology-47810367) purchased from there.  

Should we wait for the companies to eliminate DRM from the files? 
No. Companies might never remove DRM from their services. Rather, we can refuse to use services which impose DRM. You can use youtube-dl to download music files [from many sites](https://ytdl-org.github.io/youtube-dl/supportedsites.html). You can use Newpipe Android app to download music and videos from YouTube. For educational resources, we can use Libgen or Scihub to download the books and academic papers. Check the [DRM-free guide by FSF](https://www.defectivebydesign.org/guide) for websites which provide DRM-free content.  

Everytime you pay for a DRM product, the companies building such products get funds, either by your data or you pay them directly. The measure of refusing to use DRM services brings change to an extent-- that we do not fund such practices.
But the real change is to have a model for authors/creators to get funded and they release the content without DRM. 

We can fund artists without DRM. Fans can directly fund artists for their work. A lot of YouTubers get money from their Patreons and people do directly fund creators. 

Here are a few examples to demonstrate that the model can work: 

- Louis C.K. released copies of his film without any restrictions or DRM and [the film got profit in 12 hours](https://web.archive.org/web/20120516115209/http://buy.louisck.net/news/a-statement-from-louis-c-k).   

-  Diesel Sweeties released a DRM free webcomics and [had huge success](https://web.archive.org/web/20120130163103/http://www.dieselsweeties.com/blog/?p=740). They started releasing DRM-free payment optional model since then.

- The band Radiohead released an album on their website without any restrictions with [listeners allowed to pay nothing or any amount they would like to](https://www.telegraph.co.uk/finance/markets/2816893/Radiohead-challenges-labels-with-free-album.html). 

Therefore, we can fund artists directly and copy creative works will freedom. It is possible to have both freedom and creative work. What do you think?   
