--- 
title: "Choosing a privacy-respecting chatting app" 
author: "Ravi Dwivedi" 
draft: false
type: "post"
showTableOfContents: true
date: 2021-07-12
-------  

Which chatting app do you use to contact your loved ones? 

Do you use WhatsApp? 

Do you use Telegram or Signal? 

What if you do not agree to the terms and conditions or privacy policy of these apps? 

Well, either you need to accept their terms or switch to any other app and do the hard work of convincing every contact to move to your new chat app whose terms you can agree. What if you are a student and all the important notifications are sent to the WhatsApp group? Would you convince your school to avoid WhatsApp? What if they don't care? Now you are forced to be on WhatsApp to make sure you don't miss any important updates. So, when you click on 'I Agree' to the document which says, "We will put you in surveillance. It has a lot of benefits. Your life will be very convenient", [do you really agree](https://www.theguardian.com/technology/2014/sep/29/londoners-wi-fi-security-herod-clause), or you just gave in to social pressure? Or you did not care? Do you really have a choice? 


<figure>
<img
     src="/images/xkcd_messaging.png"
     alt="SMS is just the worse but I' having trouble convincing people to adopt my system, TLS IRC with a local serverand a patched DOSBox gateway running in my mobile browser." width="300" height="300" >

<figcaption>  Why SMS refuses to die. Source: https://xkcd.com/2365/ </figcaption>
</figure>



So, how do we control our means of communications? What does controlling our communications mean? If we control our means of communications, can we ensure privacy as well? I will go into details of what I mean when I say that the users control the software. If the users have the freedom to run, copy, distribute, study, change and improve the software, then the users control the software. Such a software is called free software, where 'free' refers to freedom and [not to price](https://play.google.com/store/apps/details?id=com.dealerinspire.conversations&hl=en&gl=US). In this article, 'free' refers to freedom and never to price. I suggest you to read [this article](https://www.gnu.org/philosophy/free-software-even-more-important.html) to understand why these freedoms are important and how this gives users control over the software. Examples of free software(freedom-respecting) chatting apps are [Telegram](https://telegram.org/), [Signal](https://signal.org), [Quicksy](https://quicksy.im), [Element](https://element.io) etc. If users lack any of these freedoms, then the software is called nonfree/proprietary and such a software [cannot be trusted by the user](gnu.org/malware). WhatsApp is an example of nonfree/proprietary software.

<figure>
  <img src="/images/four_freedoms.png" alt="Four Freedoms of free software" width="300" height="300" >
  <figcaption> Image: Free Software respects your freedom. Source: <a href="https://static.fsf.org/nosvn/RMS_Intro_to_FS_TEDx_Slideshow.odp">Richard Stallman's Ted talk slides</a> released under CC-BY 3.0 license. </figcaption>
</figure>

<br>

Chatting apps usually have two components: 1. The app that you install on your device; 2. A server (we will call it a service provider)which transfers the messages from the sender to the recipient. If you control only the software, the service provider still has the power to impose unjust conditions on you. In the above-mentioned example, Signal is a free software which includes the freedom to modify the code but when a project modified the Signal app code, Signal [refused to allow them to connect to their server or federate](https://github.com/LibreSignal/LibreSignal/issues/37#issuecomment-217211165) with any other server. This is not true freedom. So, to control our chatting system, software must be free but that is not enough. 

Therefore, for full control, we need to have federated chat systems -- to allow users registered on different service providers to communicate with each other - for instance a mail server run by Google federates with a mail server run by Microsoft when you send email from @gmail.com to @hotmail.com. So you can choose a free software and a trusted, community-run service-provider, and this is what I mean by having control over our communications. This control is collective control by community. Examples of such systems are [Matrix](matrix.org) and [XMPP](xmpp.org). Federation answers the question raised earlier: What to do if the service provider imposes terms and conditions you do not agree with? You can switch to another service provider or you can be a service provider and still communicate with your contacts. You don't need to convince them to switch to a new provider.  Examples of matrix apps are- [Element](element.io), [Nheko](https://nheko-reborn.github.io/), [Fluffychat](https://fluffychat.im/) etc. Examples of XMPP apps are- [Conversations](conversations.im), [Dino](https://dino.im/), [Gajim](https://gajim.org/), [Siskin IM](https://siskin.im/) etc.  Make sure that the app supports end-to-end encryption--which means only the sender and recipient can read the messages. 

<figure>
<img
     src="/images/shredder.png"
     alt="Drawing: Paper is passed through a device which shreds it into fragments. The fragments travel some distance and then are re-assembled by another device" width="300" height="300" >

<figcaption> End-to-end encryption. Credits: <a href="https://cryptpad.fr">Cryptpad</a> </figcaption>
</figure>

<br>

[Quicksy app](https://quicksy.im) is at the intersection of freedom and convenience. It registers the account with a phone number, which makes the app convenient and easy to use. It federates with XMPP so no one is forced to use Quicksy and so other people can use some other XMPP app(like Conversations) which do not require any personal details to create an account.  

To get started with XMPP, you can use [this guide](/posts/xmpp-guide) or use [Quicksy](https://quicksy.im) app if you don't mind registering with your phone number.

Telegram and Signal apps are freedom-respecting but since they are not federated, the overall chatting system is not freedom-respecting. 

Another way to freedom is to use freedom-respecting encrypted messaging apps which do not involve any servers. These are called peer-to-peer apps. The downside here is that both the users need to be online at the same time to exchange messages. You can choose to run the app in background to receive messages. Examples: [Briar](https://briarproject.org/), [GNU Jami](https://jami.net), [Tox](https://tox.chat/). 

**TL;DR** : Free Software app + Federated chat systems like [Matrix](matrix.org) or [XMPP](xmpp.org) and free software peer-to-peer apps like Briar give users full control over their communications and therefore you can ensure privacy. Nonfree/proprietary software control the users and therefore cannot be trusted for privacy. 

### Further Reading:

- [FSF India's article](https://fsf.org.in/article/better-than-whatsapp) on the same.

- [It’s all about choices and control](https://xmpp.org/2015/01/its-all-about-choices-and-control/).

- Instant Messaging: It's not about the app - [XMPP provides sovereignty of your communication](https://xmpp.org/2021/01/instant-messaging-its-not-about-the-app/).

- [How to ensure your Instant Messaging solution offers users privacy and security](https://www.erlang-solutions.com/blog/how-to-ensure-your-instant-messaging-solution-offers-users-privacy-and-security/).
