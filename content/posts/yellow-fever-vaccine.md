---
title: "Yellow Fever Vaccine"
date: 2024-07-13T13:26:03+05:30
draft: false
type: "post"
tags: ["travel", "yellow-fever", "vaccine", "kenya", "africa"]
showTableOfContents: true
---
Recently, I got vaccinated with [yellow fever](https://en.wikivoyage.org/wiki/Yellow_fever) vaccine as I am planning to travel to Kenya, a high risk country for yellow fever, in the near future. It should be taken at least 10 days before getting into the areas with yellow fever transmission to provide enough time for the formation of the antibodies. In order to get vaccinated, I searched for vaccination centers in Delhi and stumbled upon [this page](https://ihpoe.mohfw.gov.in/vaccination_centres.php) by the Indian government, which lists vaccination centers for yellow fever all over India. From that list, I made a phone call to the Airport Health Organization, a vaccination center near the Delhi Airport.

They asked me to write an email stating that I need yellow fever vaccination. After sending the email, they prompted me to attach a scanned copy of my passport data page, upon sending which they emailed me my appointment date, asking me to pay 300 INR in advance along with other instructions. The appointment date was 4 days after I sent scanned copy of my passport. The email also mentioned that those who are allergic to eggs or have never taken eggs should instead visit RML Hospital. 

The vaccination center must be visited during 10 AM to 12 noon on the date of appointment. I reached there at around 11 AM and got vaccinated in around 40 minutes, followed by obtaining a vaccine certificate in half an hour. 

One dose of this vaccine gives immunity against yellow fever for lifetime. Therefore, I can travel to any country with yellow fever transmission after getting this dose. Although some countries may require proof of vaccination within some time frame and some people might need a booster dose to maintain immunity.
