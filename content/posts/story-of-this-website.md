---
title: "Story of creation of this website"
date: 2021-09-28
draft: false
type: "post"
showTableOfContents: true
---

This is the story of creation of the website you are reading and how it was created. 

I always wanted to create a website from as far as I can remember. This is because I wanted autnonomy over the content that I can post. Social media posting format is not suitable for website like this. Also the website has a cool domain on my name which feels really good.

In the Software Freedom Camp 2020, I met [Sahil](https://sahilister.in) and [Arun](https://arunmathaisk.in). Sahil already had a website, while Arun purchased a domain name around that time. I got inspired from Arun and bought this domain from gandi.net in October 2020. 

Then I took the project in Free Software Camp 2020 to learn Hugo. Didn't learn much from there as I didn't put a lot of time and effort into it. 

After the domain purchase, the website wasn't up for 9 months. Finally, in July 2021, Sahil helped me set up this website. And after tinkering with the website, I learnt enough Hugo and git to maintain the website. And here I am, writing the story of the website.  
