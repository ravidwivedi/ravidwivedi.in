---
title: "Fixing audio and keymaps in Chromebook Running Debian Bookworm"
date: 2023-09-26T13:07:22+05:30
tags: ["debian", "chromebook", "fix"]
type: "post"
showTableOfContents: true
draft: false
---
I recently bought an HP Chromebook from Abhas who had already flashed coreboot in it. I ran a fresh installation of Debian 12 (Bookworm) on it with KDE Plasma.

Right after installation, the Wi-Fi and bluetooth were working, but I was facing two issues:

- Playing a music file or any audio file does not give any audio.

- Keyboard buttons like the ones for brightness and audio adjustment were not working (alphabet keys were working).

## Fixing audio

I ran the script mentioned [here](https://github.com/WeirdTreeThing/chromebook-linux-audio) and that fixed the audio.

The instructions from that link are:

```
git clone https://github.com/WeirdTreeThing/chromebook-linux-audio
cd chromebook-linux-audio
./setup-audio
```

## Fixing keyboard

To fix the keyboard, go to KDE Settings and in the Hardware section, under the "Keyboard model" box select the option `Google | Chromebook` as depicted in the screenshot below.

{{< figure loading="lazy" src="/images/keyboard-layout-chromebook.png" title="Screenshot depicting keyboard layout settings of KDE." width="500">}}

I hope this post fixed the issues for you. Meet you in the next post.
