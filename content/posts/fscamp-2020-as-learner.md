---
title: "My experience as a learner in Free Software Camp 2020"
draft: false
type: "post"
date: 2021-09-27
tags: ["freesoftware", "camp"]
showTableOfContents: true
---
### Index

- [Introduction](#introduction)

- [What is Free Software Camp](#what-is-free-software-camp)

- [Motivation to join the camp](#motivation-to-join-the-camp)

- [How I came to know about the camp](#how-i-came-to-know-about-the-camp)

- [Experience as a learner in the camp](#experience-as-a-learner-in-the-camp)

- [Aftermath](#aftermath)

- [Word for learners of the upcoming camps](#word-for-learners-of-the-upcoming-camps)

### Introduction

I participated as a learner in [Free Software Camp 2020](https://camp.fsf.org.in) organized by [FSCI](https://fsci.in) and [FSF India](https://fsf.org.in). This is a post about my experience of the same. If you don't know what free software is, then you can [read my article on the same](/posts/free-software-explained-simply). 
I have never attended any software-related camp other than this one and therefore I have no idea what other camps are about and so I cannot compare this camp with any other camp of technical nature or one which involves introducing free software philosophy to the participants. Since the camp concluded a long before I am writing this post, there is a chance I forgot a lot of things about it or my feelings/opinions of many things at that time.  

### What is Free Software Camp

This is my takeaway for what free software camp means. I think that the camp had two goals:

- To acquaint the participants with [free software philosophy](https://www.gnu.org/philosophy/free-software-even-more-important.html).

- To connect the learners with mentors and contribute to free software projects. These contributions don't have to be technical in nature but they must contribute to free software in some way. 

The camp was divided into the following phases: 

- Ice breaking sessions.

- Introduce the learners with free software philosophy. 

- GNU/Linux Installation phase.
	
- A phase where we get to meet people who earn their living from free software.

- Projects phase.	

### Motivation to join the camp

I was [already introduced to free software philosophy](/posts/meeting-rms/) and started caring about the idea in 2020. The main thing I was looking at the time around September 2020 was: How does free software actually work in reality? I was not aware of communities that power free software. Curiosity to know how free software works was one motivation to join the camp. 

Also, before the start of the camp, I already started shifting to free software but I needed help on: 1. Which software do people from free software community use? ; 2. To try and test these software. 

For example, I came to know about BigBlueButton from the camp. It is a videoconferencing software which respects freedom and allows self-hosting.

Another motivation was to meet with like-minded people because usually people don't get excited about free software and therefore, one can get demotivated.

To sum up, my motivations were:

- To understand how free software communities work.

- To know what software free software proponents use.

- To meet like-minded people. 

My motive was not really learning anything technical.  

### How I came to know about the camp

The camp was announced in mid September 2020. And a few days ago, I filled the volunteer form for FSF India. Due to that group's announcement, I came to know about the camp. 

### Experience as a learner in the camp

I registered for the camp and started joining the sessions when the camp commenced. In the first stage, the learners were divided into small groups based on their language preference. I was added to Group 6 which was a Hindi group and Sruthi was assigned to our group from the organizing team as a co-ordinator(Sorry I cannot find a word for this). 

I mixed up well with this group. We used to have regular meetings and shared what we think about various issues. People used to feel autonomous in sharing their thoughts. And we also helped each other in various things, for example, I helped people in setting up their gpg keys and taught how to send encrypted emails. So, it can be said that Group 6 ice-breaking session really broke the ice or maybe the ice was not even there to begin with. Groups were also given an activity to present on a topic assigned by the organizers. Our group decided to have an audio presentation on the topic. The topic was something related to democracy and digital rights(how surveillance, censorship etc. affects democracy). We all rehearsed it many times, suggested each other of points and finally completed the recording and submitted it for the presentation in a camp session. I found that overall, be it my group, other learners, organizers, or mentors, people were very helpful, kind and inclusive. 
This ended our ice-breaking sessions. 

Next was GNU/Linux installation phase. I used to have Macbook at that time. I could boot GNU/Linux in my Macbook but the touchpad didn't work. So that was a bit sad. I badly wanted to run a GNU/Linux distro on my Macbook. Maybe external mouse would have worked. But I didn't try that. Therefore, I proceeded in the camp with MacOS installed. 

Next was the phase where we [met people who earn their living from free software](https://videos.fsci.in/videos/watch/cca5981d-8513-4448-8cb6-195f2c9db648). I [attended sessions by Abhas](https://videos.fsci.in/videos/watch/75deb449-9a12-4c45-8ec3-a6990d14f675) and [Nagarjuna](https://videos.fsci.in/videos/watch/9f98c928-2158-4ebf-b526-186bc1817b56). They were good and gave me some insights. Abhas has a way of making sessions interactive which I would like to do in my own sessions(not only in camp but anywhere). Nagarjuna raised issues about copyright mainly and was very interactive, which I really liked. He was in no hurry but asked questions from us and patiently waited for our responses and cleared doubts. 

Now came the projects phase. Initially, I was not excited to do any technical project. So I thought of choosing a non-technical project. I ended up choosing a technical one because I wanted to learn how to create website so that I can create my own wesbite. I chose the project offered by [Karthik](https://kskarthik.gitlab.io) to learn Hugo Static Site Generator to create websites. The project was to create and maintain Privacy Yathra website. I took the project as a long-term work rather than just set up the website as a part of the camp and run away.

I didn't work a lot on the project and I could not learn Hugo within the deadline set by the camp. But later on, I set up this website you are reading, with the help of [Sahil](https://sahilister.in). From there on, I learnt from different people about different things about creating and maintaing the website. The Privacy Yathra website(which was the project of the camp) is not yet created but as now I know how to work with Hugo and git(actually I still don't know much), I would one day be up. Keep looking for Privacy yathra website, folks :) It can be up anytime. 

Shout out to all the people who made this work so smoothly. Especially organizers were always available for any help I needed and were very welcoming. 
### Aftermath

After the conclusion of the camp, I am integrated with FSCI as a campaigner. As mentioned above, I set up my website after the camp. Also, I am a part of the organizing team of the succeeding camp named "[Software Freedom Camp Diversity Edition 2021](https://camp.fsci.in)" organized by [FSCI](https://fsci.in). This year(2021) we are looking for diversity. We are focusing on reaching out to groups underrepresented in the free software community.  Registrations for learners are open till 15th October. Please [register](https://camp.fsci.in) if you are interested and from an underprivileged backgroud. Please help us raise awareness about the camp so that we can reach the needy. You can [share the camp poster](https://cdn.masto.host/floss/media_attachments/files/106/982/355/338/001/276/original/0414e3a24fca6cea.png) on social media(even on Twitter, Facebook, Instagram, if you use them anyway, then why not?), with your contacts in chat, emails etc. 

### Word for learners of the upcoming camps

If you are participating as a learner in any of the editions of the camp, then I have some advice for you. Hopefully, the above text gave you some idea on what happens in the camp. I think the best way to contribute to the camp is to contribute to free software. You might be surprised that the best way to contribute to free software is not about adding code or features to free software, although they are very good contributions. I think it is not a good idea to improve the code of some free software or work on a free software project for a few months and then forget about free software. I would suggest you to understand what free software philosophy is, why free software is important, use free software in your own lives for your own work and the hardest part, try to convince others to use free software and avoid proprietary software. If you are committed to free software philosophy, you will develop free software and contribute to free software as a side effect. If you are not committed to free software philosophy, then you will only contribute code to free software as a part of the camp and then forget about it. Take it as a lifetime project to contribute to free software to switch to free software and writing code to develop or improve the free software. Also, avoid proprietary software.  
