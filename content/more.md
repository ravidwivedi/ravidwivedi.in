---
title: "More"
date: 2023-10-20T11:22:45+05:30
type: "page"
draft: false
---
- [My social profiles](/social)

- [My Talks](/talks)

- [Free Software](/free-software)

- [Hardware Freedom](/hardware-freedom)

- [Books](/books)
