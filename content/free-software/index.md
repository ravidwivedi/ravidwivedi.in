---
title: "Free/Libre/Swatantra/Mukt Software"
date: 2021-11-21
type: "post"
showTableOfContents: true
draft: false
---
<figure style="float:left;margin:0px 50px 50px 0px">
<img src="https://upload.wikimedia.org/wikipedia/commons/a/a4/4-freedoms-poster.png"  style="width:256px;"> 
<figcaption>Credits:<a href="https://www.gnu.org/graphics/amihud-4-freedoms.html">Jeison Yehuda Amihud</a></figcaption>
</figure>

This page contains resources on freedom-respecting software. 

Check [Free Software Explained Simply](/posts/free-software-explained-simply) for an introduction on Free Software.

Free Software means software gives users all the below mentioned freedoms: 

- Freedom 0: Freedom to run/use the software for any purpose;

- Freedom 1: Freedom to study/inspect and modify/adapt the software. 

	Users must have [source code](https://en.wikipedia.org/wiki/Source_code) of the software to exercise this freedom; 

- Freedom 2: Freedom to share the software;

- Freedom 3: Freedom to share modified versions of the software. Users must have [source code](https://en.wikipedia.org/wiki/Source_code) of the software to exercise this freedom; 

When I say the term 'Free Software', I never refer to free-of-cost software. Free Software is a [matter of liberty, not price](https://en.wikipedia.org/wiki/Gratis_versus_libre). Therefore, I sometimes say libre software or swatantra software or mukt software, because the last two terms work well in many Indian languages. An example of a free software which you get by payment is [Conversations app](https://play.google.com/store/apps/details?id=eu.siacs.conversations&referrer=utm_source%3Dwebsite) on Play Store.  

If the software fails to provide any of these freedoms, it is called nonfree/proprietary software. 

Read [Why Free Software is important](/posts/free-sw) to understand why these freedoms must be the right of every computer user.

A few examples of Free Software are: GNU/Linux distros like Ubuntu, Debian, Arch, Fedora etc, Firefox browser, VLC Media Player, Thunderbird. 

Some examples of nonfree/proprietary software are: Microsoft Windows, Apple MacOS, iOS, Adobe Photoshop, Google Chrome etc. 

To use Free Software, please check: 

- [F-Droid app store](https://f-droid.org): F-Droid app store for Android which contains only Free Software.

- My [Free Software list](/posts/free-software-list).

- [List of Free Software for desktop](https://git.fosscommunity.in/community/camp/-/wikis/List-of-Free-Softwares-in-Desktop).

- [FSF Directory of Free Software](https://directory.fsf.org/wiki/Main_Page).

Proprietary Software is not controlled by users but controlled by the developer of the porgram. Check out [Proprietary software is malware](https://gnu.org/malware) for illustration of how users are abused by proprietary software. Therefore, I advice you to avoid nonfree/proprietary software as much as possible.  

If you are thinking of what is the difference between open source software and free software, then check out [my blog post on that issue](/posts/fs-and-oss-same). While they refer to broadly the same set of software, the main difference lies in the values of their movements. Pirate Praveen's quote explains the difference succintly, "Open Source wants to create better software, Free Software wants to create a better society,", keeping in mind that Pirate Praveen is a Free Software proponent(and me too). Feel free to form your own opinions and use the term you prefer. 

In GNU/Linux distros, you can use [vrms package](https://en.wikipedia.org/wiki/Vrms), available for many GNU/Linux distros, to check whether there any nonfree software are installed in your computer.

Articles on free software: 

- [Free Software is necessary for privacy](/posts/free-software-important-for-privacy).

- [When Free Software saved the day](/posts/scribus).

- [Why Software should be free(as in 'freedom')](https://www.gnu.org/philosophy/shouldbefree.en.html).

- [Freedom in network-based services](https://fsci.in/blog/self-hosting-and-federation/). 

Finally, I use only Free Software for all my computing and I am proud of it. You can do it too. 

<img src="/images/vrms.png" width="900" height="200">

Why not install a GNU/Linux distro in your computer and kick out Microsoft Windows?

If you would like to download a GNU/Linux distro, I recommend [Debian GNU/Linux](https://www.debian.org/intro/why_debian), [PureOS](https://pureos.net/), [Trisquel GNU/Linux](https://trisquel.info/) or any of the [distros recommended by GNU](https://www.gnu.org/distros/free-distros.en.html).

I myself run PureOS or Debian. 
