---
title: "GPG Keys"
date: 2022-06-14
type: "page"
---
Check out an [introduction to gpg](https://wiki.archlinux.org/title/GnuPG#Create_a_key_pair) if you are not sure what gpg keys are.

Text file of my public GPG keys are [here](/files/ravidwivedi.asc). Creation date: 13-November-2022.

To import my key, run the following command:

`curl 'https://ravidwivedi.in/files/ravidwivedi.asc' | gpg --import` 

My key fingerprint is: 

`FF7D B951 7CE1 E19B 6EFE 695F E0E5 BAFD 3BBF 70B3` 
