---
title: "Glossary"
date: 2021-07-25
---

(Last Updated: Thursday 31 March 2022) 

Here are the definitions of the terms I use. If a nontechnical regular computer user fails to understand these terms, then I have failed. 

### Index

- [Backdoor](#backdoor)
- [Blobs](#blobs)
- [Centralized Services](#centralized-services)
- [End-to-end encryption](#end-to-end-encryption)
- [Federated Services](#federated-services)
- [Firmware](#firmware)
- [Free Format](#free-format)
- [Free Software](#free-software)
- [Interoperability](#interoperability)
- [Malware](#malware)
- [Nonfree/Proprietary Software](#nonfreeproprietary-software)
- [Vendor Lock-in](#vendor-lock-in)

#### Backdoor
A backdoor means someone can remotely access your system, like the software developer itself or any third-party bypassing normal authentication or encryption in a computer. For example, a backdoor [was found in Microsoft Windows](http://www.informationweek.com/microsoft-updates-windows-without-user-permission-apologizes/d/d-id/1059183) through which any change whatsoever can be imposed on the users.

#### Blobs
Pieces of object code distributed without source, usually [firmware](/posts/firmware) to run some device.

#### Centralized Services 
A service in which every user is on the same server or network of servers controlled by the same organization. Example: Signal, Twitter, Google Meet, Facebook, YouTube etc. 

#### End-to-end Encryption 
Only users exchanging message/information can read them. 

#### Federated Services 
Federation allows separate deployments of a communication service to communicate with each other - for instance a mail server run by Google federates with a mail server run by Microsoft when you send email from @gmail.com to @hotmail.com. Example: [Matrix](https://matrix.org), XMPP, Mastodon, PeerTube. 

#### Firmware
Firmware is a software which provides a way for hardware to interact with the operating system. It runs on a secondary processor and not on the CPU. For example the WiFi chipset will run this code directly instead of the main CPU. For more details, please [check my post on this topic](/posts/firmware/).

#### Free Format
Free format is a digital file format whose specifications are public so that anyone can develop a software to view files in that format. For example, FLAC for music files, WebM for video files, PDF for reading files, etc. There are so many PDF readers because it is a free format and anyone is allowed to develop a software to read PDFs. Some examples of formats which are not free: AAC for music, Kindle File format for e-books etc.

#### Free Software
[Free Software](/free-software) means that the users have the freedom to run, copy, distribute, study, change and improve the software. Example: GNU/Linux distros, Firefox browser, Jitsi Meet, Madtodon etc. Check out my [Free Software list](https://ravidwivedi.in/free-software-list) for more examples.

#### Interoperability
Priya gets a funny meme sent to her on Instagram. She wants to forward it to Faizal. But Faizal doesn't have an Instagram account, although they do have a WhatsApp account. Priya downloads the meme to her phone and sends it to Faizal on WhatsApp. If WhatsApp and Instagram were interoperable she could have sent it to them directly from Instagram. 

(Credits to [Akshay](https://asd.learnlearn.in/) for the above example).

Interoperability means users using different services can communicate.

#### Malware
Software whose functioning mistreats the user is called malware. 

#### Nonfree/Proprietary Software 
Any software which is not a [free software](/posts/free-sw). A nonfree software does not respect users' freedom. Example: Microsoft Windows, Adobe Photoshop, WhatsApp etc. 

#### Vendor Lock-in
Vendor lock-in means that the cost of switching to a product of another vendor is very high for the user. This makes user dependent on the product. 

WhatsApp is an example of vendor lock-in. For WhatsApp users, switching to another chatting app comes at a high cost - losing all their contacts and convincing each of them to switch. Similarly, Signal and Telegram are also examples. 
